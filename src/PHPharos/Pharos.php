<?php

namespace PHPharos;

use PHPharos\Commons\Strings;

use PHPharos\Exceptions\HttpException;

use Composer\Autoload\ClassLoader;

use PHPharos\Commons\File;
use PHPharos\Logger\Logger;

class Pharos {
	
	private static $classLoader;
	
	private static $currApp;
	
	
	public static function errorHandler($errno, $errstr, $errfile, $errline){
		throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
	}
	
	/**
	 * @return ClassLoader
	 */
	public static function getClassLoader(){
		return self::$classLoader;
	}
	
	public static function getApplications($path){
		$apps_path = new File($path."/apps");
	
		$dirs = $apps_path->findFiles();
		$apps = array();
		/* @var $dir \PHPharos\Commons\File */
		foreach($dirs as $dir){
			if ($dir->isDirectory()){
				$conf = new File("conf/application.conf", $dir);
				if ($conf->exists()){
					$apps[] = new Application($dir);
				}
			}
		}
	
		return $apps;
	}
	
	/**
	 * @return \PHPharos\Application
	 */
	public static function getCurrent(){
		return self::$currApp;	
	}
	
	
	public static function run($root, ClassLoader $loader){
		set_error_handler(__CLASS__."::errorHandler", E_ALL);
		
		self::$classLoader = $loader;
		
		$apps = self::getApplications($root);
		
		/* @var $app \PHPharos\Application */
		$app_matched = null;
		$last_rule = null; 
		foreach($apps as $app){
			$rule = $app->getCurrentRule();
			if ($rule){

				if ($last_rule == null || $rule->constraints($last_rule)){
					$last_rule = $rule;
					$app_matched = $app;
				}
			}
		}

		if ($app_matched != null)
			self::launchApplication($app_matched);
	
	}
	
	protected static function launchApplication(Application $app){
		self::$currApp = $app;
		self::loadApplication($app);
		self::processRequest($app);
	}
	
	protected static function loadApplication(Application $app){
		self::getClassLoader()->add('', $app->getPath()."/src");
		$app->registerLogger();
		
		$bootstrap_class = "\\Bootstrap";
		$bootstrap = null;
		
		if (class_exists($bootstrap_class)){
			/* @var $bootstrap \PHPharos\AbstractBootstrap */ 
			$bootstrap = new $bootstrap_class($app);
			$app->setBootstrap($bootstrap);
			$bootstrap->onBeforeLoad();
		}
		
		$includes = $app->getConfig()->getArray("app.include.classes", array());
		foreach ($includes as $path) {
			$inc = new File($path, $app->getPath());
			self::getClassLoader()->add('', $inc->getPath());
		}		
		
		$app->registerRoutes();
		$app->registerActiveRecord();
		$app->registerFenom();
		
		if ($bootstrap)
			$bootstrap->onAfterLoad();
	}
	
	protected static function processRequest(Application $app){
		$router = $app->getRouter();
		$router->route();
		
		/* @var $response \PHPharos\Http\Response */
		
		try {
			if (!$router->action){
				throw new HttpException(HttpException::E_NOT_FOUND, 'Action not found for requsted URI: '.$_SERVER['REQUEST_URI']);
			}
	
			// TODO optimize ?
			$tmp = explode('.', $router->action);
			array_walk($tmp, function(&$item, $key){
    			$item = ucfirst($item);
			});
						
			$controllerClass = implode('\\', array_slice($tmp, 0, -1));
			$actionMethod    = $tmp[ sizeof($tmp) - 1 ];
			
			if (!class_exists($controllerClass)){
				throw new HttpException(HttpException::E_NOT_FOUND, "Class '".$controllerClass."' not found");
			}
			
			/* @var $controller \PHPharos\Controller */
			$controller = new $controllerClass();
	
			//$controller = new $controllerClass;
	
			$controller->actionMethod = $actionMethod;
			$controller->routeArgs    = $router->args;
	
			try {
				$reflection = new \ReflectionMethod($controller, $actionMethod);
				$controller->actionMethodReflection = $reflection;
			} catch(\ReflectionException $e){
				throw new HttpException(HttpException::E_NOT_FOUND, $e->getMessage());
			}
	
			$declClass = $reflection->getDeclaringClass();
	
			if ( $declClass->isAbstract() ){
				throw new \RuntimeException(Strings::format('Can`t use the "%s.%s()" as action method', $controllerClass, $actionMethod));
			}
	
			if ( !$reflection->isPublic() ){
				throw new \RuntimeException(Strings::format('Can`t use the "%s.%s()" as action method', $controllerClass, $actionMethod));
			}
			
			/*
			if ($app->getBootstrap())
				$app->getBootstrap()->onBeforeRequest($request);
			*/
			
			//SDK::trigger('beforeRequest', array($controller));
	
			$controller->callBefore();
			//Regenix::trace('Process request preparing - finish.');
	
			$return = $router->invokeMethod($controller, $reflection);
	
			// if use return statement
			$controller->callReturn($return);
	
		} catch (Result $result){
			$response = $result->getResponse();
		} catch (\Exception $e){

			$response = self::catchException($e, isset($controller) ? $controller : null);
		}
	
		if ( isset($controller) ){
			$controller->callAfter();
		}
	
//		if ($app->getBootstrap())
//			$app->getBootstrap()->onAfterRequest($request);
	
		if ( !isset($response) ){
			throw new \RuntimeException(Strings::format('Unknown type of action `%s.%s()` result for response',
					$controllerClass, $actionMethod));
		}
	
		
		$response->send();
		
		
		if ( isset($controller) ){
			$controller->callFinally();
		}
		
		
		
//		if ($app->getBootstrap())
//			$app->getBootstrap()->onFinallyRequest($request);
	
		return $response;
	}
	
	public static function catchException(\Exception $e, Controller $controller = null){
		Logger::error($e);
		
		$app = self::getCurrent();
		
		if ($e instanceof HttpException){
			if ( isset($controller) )
				$controller->callHttpException($e);
			$responseErr = $e->getTemplateResponse();
		}
			
		if (!isset($responseErr))
			try {
				// if no result, do:
				if (isset($controller))
					$controller->callException($e);
				if ($app->getBootstrap())
					$app->getBootstrap()->onException($e);
			} catch (Result $result){
				/** @var $responseErr Response */
				$responseErr = $result->getResponse();
			} catch (\Exception $new){
				$responseErr = self::catchException($new, $controller);
			}
		
		
		if ( !isset($responseErr) )
			throw $e;
		
	
		return $responseErr;
	}
}

