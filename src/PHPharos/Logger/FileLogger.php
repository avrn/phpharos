<?php

namespace PHPharos\Logger;

class FileLogger extends AbstractLogger {
	
	private $log_file;
	
	/**
	 * Constructor
	 * @param String log_file - Absolute file name/path.
	 * @return void
	 **/
	function __construct($log_file = null) {
		$this->log_file = $log_file;
	
		if(!file_exists($log_file)){ //Attempt to create log file
			touch($log_file);
		}
	
		//Make sure we'ge got permissions
		if(!is_writable($log_file)){
			//Cant write to file,
			throw new \Exception("LOGGER ERROR: Can't write to log", 1);
		}
	}
	
	
	protected function writeToLog($message) {
		$message = trim($message) . PHP_EOL;
		file_put_contents($this->log_file, $message, FILE_APPEND);
	}
	
}
