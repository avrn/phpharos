<?php

namespace PHPharos\Logger;

use PHPharos\Pharos;

abstract class AbstractLogger {
	
	const INFO = 'INFO';
	const DEBUG = 'DEBUG';
	const WARNING = 'WARNING';	
	const ERROR = 'ERROR';	
	
	
	protected abstract function writeToLog($message);
	
	public function debug(){
		$this->log(AbstractLogger::DEBUG, func_get_args());
	}
	
	
	public function error(){
		$this->log(AbstractLogger::ERROR, func_get_args());
	}
	
	
	public function warning(){
		$this->log(AbstractLogger::WARNING, func_get_args());
	}
	
	
	public function info(){
		$this->log(AbstractLogger::INFO, func_get_args());
	}
		
	
	public function log($level, $args){
		$message = $this->buildMessage($args);
		$this->write($level, $message);
	}
	
	
	private function write($level, $message){
		$level_str = "INFO";
		
		switch ($level){
			case AbstractLogger::INFO:
				$level_str = "INFO"; break;
			case AbstractLogger::DEBUG:
				$level_str = "DEBUG"; break;
			case AbstractLogger::WARNING:
				$level_str = "WARNING"; break;
			case AbstractLogger::ERROR:
				$level_str = "ERROR"; break;
		}
		
		//$date = date_create_from_format('U.u', microtime(true))->format('Y-m-d H:i:s.u'); 
		$date = date('Y-m-d H:i:s').substr(microtime(), 1, 8);
		
		$app = Pharos::getCurrent();
		
		$message = "$date -- [".$app->getName()."] [$level_str] $message" . PHP_EOL;
		$this->writeToLog($message);
	}
	
	
	
	private function buildMessage($args){
		$message = '';
		
		if (count($args) > 0){
			$message = self::getStringValue($args[0]);
			unset($args[0]);
	
			foreach($args as $value){
				$pos = strpos($message, "{}");
				if ($pos !== false)
					$message = substr_replace($message, self::getStringValue($value), $pos, 2);
				else
					break;
			}
		}
		
		return trim($message);
	}
	
	
	private function getStringValue($value){
		$trace_tab = "\t\t\t";
		
		switch(true){
	
			case is_bool($value):
				return $value === true ? "true" : "false";
	
			case ($value instanceof \Exception):
				return 	get_class($value).': '.$value->getCode() .' - "'.$value->getMessage().
				'" on file "'.$value->getFile().
				'", line: '.$value->getLine().PHP_EOL.
				$trace_tab.str_replace("\n", PHP_EOL.$trace_tab, $value->getTraceAsString());
	
			case is_null($value):
				return "null";
	
			case is_object($value):
				return get_class($value);
	
			case is_array($value):
				ob_start();
				print_r($value);
				return ob_get_clean();			
				//return "Array[".count($value)."]";
	
			default:
				return $value;
		}
	
	}
}

?>