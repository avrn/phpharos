<?php

namespace PHPharos\Logger;


class Logger {

	private static $logger;
	private static $levels = array('info', 'warning', 'debug', 'error');
		
	public final static function init(AbstractLogger $logger){
		self::$logger = $logger;
	}
	
	
	private final static function getLogger($level){
	
		if (!isset(self::$logger) || array_key_exists(strtolower($level), self::$levels)){
			return false;
		}
	
		return self::$logger;
	}
	
	public final static function warning() {
		$logger = self::getLogger(AbstractLogger::WARNING);
		
		if ($logger)
			$logger->log(AbstractLogger::WARNING, func_get_args());
	}
	
	
	public final static function debug() {
		$logger = self::getLogger(AbstractLogger::DEBUG);
		if ($logger)
			$logger->log(AbstractLogger::DEBUG, func_get_args());
	}
	
	
	public final static function error() {
		$logger = self::getLogger(AbstractLogger::ERROR);
		if ($logger)
			$logger->log(AbstractLogger::ERROR, func_get_args());
	}
	
	
	public final static function info() {
		$logger = self::getLogger(AbstractLogger::INFO);
		if ($logger)
			$logger->log(AbstractLogger::INFO, func_get_args());
	}
}

