<?php
namespace PHPharos\Exceptions;

use PHPharos\Pharos;

use PHPharos\Http\Response;

use PHPharos\Commons\Strings;

class HttpException extends \Exception {

    const E_BAD_REQUEST      = 400;
    const E_UNAUTHORIZED     = 401;
    const E_PAYMENT_REQUIRED = 402;
    const E_NOT_FOUND        = 404;
    const E_FORBIDDEN        = 403;
    const E_METHOD_NOT_ALLOWED = 405;
    const E_NOT_ACCEPTABLE   = 406;
    const E_CONFLICT         = 409;
    const E_GONE             = 410;
    const E_LENGTH_REQUIRED  = 411;
    const E_UNSUPPORTED_MEDIA_TYPE = 415;

    const E_INTERNAL_SERVER_ERROR = 500;
    const E_NOT_IMPLEMENTED       = 501;
    const E_BAD_GATEWAY           = 502;
    const E_SERVICE_UNAVAILABLE   = 503;
    const E_GATEWAY_TIMEOUT       = 504;

    private $status = 0;
    private $args = array();

    public function __construct($status, $message = '', array $args = array()){
    	$this->code = $status;
        $this->status = $status;
        $this->args = $args;
        
        $args = array_slice(func_get_args(), 2);
        
        if (isset($args) || count($args) == 0)
        	$message = str_replace("%", "%%", $message);
        
        parent::__construct(Strings::formatArgs($message, $args));
    }

    /** @return int */
    public function getStatus(){
        return $this->status;
    }
    
    /**
     * @return Response
     */
   
    public function getTemplateResponse(){
        $response = new Response();
        $response->setStatus($this->getStatus());

        $template = Pharos::getCurrent()->getFenom()->getTemplate('Errors/' . $this->getStatus().".html");
        $this->args['e'] = $this;
        $response->setArgs($this->args);
        $response->setEntity($template);

        return $response;
    }
}
