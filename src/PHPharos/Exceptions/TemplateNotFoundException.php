<?php
namespace PHPharos\Exceptions;

use PHPharos\Commons\Strings;

class TemplateNotFoundException extends \Exception {

	public function __construct($message = '', array $args = array()){
    	
        parent::__construct(Strings::formatArgs($message, array_slice(func_get_args(), 2)));
    }

}
