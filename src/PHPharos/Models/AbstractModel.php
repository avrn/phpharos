<?php

namespace PHPharos\Models;

use ActiveRecord\Model;
use PHPharos\Validation\Validator;
use Models\Exceptions\DataException;
use PHPharos\Validation\Exceptions\ValidationException;

abstract class AbstractModel extends Model implements Validator {
	private $props = null;
	private $isValid = false;
		
	public function save($validate = true){
		
		if (!$this->isValid && $validate){
			$v = $this->validator();
			$this->isValid = !$v->hasErrors(); 
			if (!$this->isValid)
				throw new ValidationException($v->getErrorsAsString());
		}
		
		$this->assignAttributes();
		
		
		parent::save(false);
		self::reloadAttributes($this);
		
		
		return true;
	}


	/**
	 * @return \ReflectionProperty[]
	 */
	public function getProperties(){
		if ($this->props == null){
			$class = get_called_class();
			
			$reflect = new \ReflectionClass($this);
			$all_props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
			
			$props = array();
			/* @var $p \ReflectionProperty */
			foreach($all_props as $p)
				if ($p->class == $class && !$p->isStatic()){
					$props[$p->getName()] = $p;
				}
						
			$this->props = $props;
		}
		
		return $this->props;
	}
	
	private function existsProperty($name){
		return key_exists($name, $this->getProperties());
	}
	
	
	private function assignAttributes(){
		$props = $this->getProperties();
		/* @var $p \ReflectionProperty */
		foreach ($props as $p){
			$this->assign_attribute($p->getName(), $p->getValue($this));
		}
	}
	
	public static function reloadAttributes(AbstractModel $to, AbstractModel $from = null){
		if ($from == null)
			$from = $to;
		
		if (get_class($to) != get_class($from))
			throw new DataException('Models must be instance of one class. %s != %s', get_class($to), get_class($from));
		
		$props = $to->getProperties();
		/* @var $p \ReflectionProperty */
		foreach ($props as $p){
			$p->setValue($to, $from->read_attribute($p->getName()));
		}
	}
	
	
	public function assign_attribute($name, $value){
		parent::assign_attribute($name, $value);
		
		$old_value = $this->$name;
		
		if (!isset($old_value) || $this->$name != $value)
			$this->$name = $value;
	}
	
	
	public static function modelClone(AbstractModel $model){
		$class = get_class($model);
		
		$instance = new $class();
		
		$props = $model->getProperties();
		foreach ($props as $p){
			$p->setValue($instance, $p->getValue($model));
		}
		
		return $instance;
	} 
}

