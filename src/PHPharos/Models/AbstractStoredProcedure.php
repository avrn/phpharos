<?php

namespace PHPharos\Models;

use ActiveRecord\DatabaseException;
use ActiveRecord\ConnectionManager;


abstract class AbstractStoredProcedure {
	private static $reflectionClass;
	
	const SATAIC_PARAMS = "PARAMS"; 
	const CONST_PROC_NAME = "NAME";
	
	public static function execute($args = array()){
		
		$param_prefix = "@";
		$class = get_called_class();
		$params = self::getParams($class);
		
		$call_sql = self::getSQLString($class, $params, $param_prefix).";";
		
		
		$params_sql = '';
		$fields = [];
		foreach ($params as $name => $type){
			$params_sql .= "set ".$param_prefix.$name." = :".$name."; ";
			$fields[] = $param_prefix.$name." as `".$name."`";
		}

		$select_sql = 'select '.join(', ', $fields).';';
	

		$conn = ConnectionManager::get_connection();
		/* @var $sth \PDOStatement */
		try {
			
			$sth = $conn->connection->prepare($params_sql);
			if (!$sth)
				throw new DatabaseException($conn);
			
			self::bindParams($sth, $params, $args);
				
			if (!$sth->execute())
				throw new DatabaseException($conn);
			
			$sth = $conn->connection->prepare($call_sql);
			if (!$sth)
				throw new DatabaseException($conn);
			if (!$sth->execute())
				throw new DatabaseException($conn);
			
			$sth = $conn->connection->prepare($select_sql);
			if (!$sth)
				throw new DatabaseException($conn);
				
			if (!$sth->execute())
				throw new DatabaseException($conn);
			
		} catch (\PDOException $e) {
			throw new DatabaseException($e);
		}
		
		
		//Logger::debug("Stored Procedure: {}; params: {}; time: {}", $sql, $args, (microtime(true) - $start));
		$result = $sth->fetchAll(\PDO::FETCH_CLASS, $class);
			
		return $result[0];
	}
	
	/**
	 * @param array $args
	 * @return array
	 */
	public static function getListResult($args = array()){
		$list = self::call($args);
	
		if (!isset($list))
			return array();
	
		return $list;
	}
	
	public static function getSingleResult($args = array()){
		$r = self::call($args);
		
		if (!isset($r) || !isset($r[0]))
			throw new \Exception("Function not return result");
		
		return $r[0];
	}
	
	public static function call($args = array()){
		$start = microtime(true);
		$param_prefix = ":";
		$class = get_called_class();
		$conn = ConnectionManager::get_connection();
		
		$params = self::getParams($class);
		$sql = self::getSQLString($class, $params, $param_prefix);
				
		/* @var $sth \PDOStatement */
		try {
			
			if (!($sth = $conn->connection->prepare($sql)))
				throw new DatabaseException($conn);
			
			self::bindParams($sth, $params, $args);
			
			if (!$sth->execute())
				throw new DatabaseException($conn);
			
		} catch (\PDOException $e) {
			throw new DatabaseException($e);
		}

		//Logger::debug("Stored Procedure: {}; params: {}; time: {}", $sql, $args, (microtime(true) - $start));
		
		$result = $sth->fetchAll(\PDO::FETCH_CLASS, $class);
			
		return $result;
	}	
	
	private static function bindParams(\PDOStatement &$sth, array $params, array $args = array()){
		$i = 0;
		foreach ($params as $name => $type){
			$args[$name] = isset($args[$name]) ? $args[$name] : (isset($args[$i]) ? $args[$i] : null);
			unset($args[$i]);
				
			if ($args[$name] == null)
				$type = \PDO::PARAM_NULL;
		
			if (!$sth->bindValue(":".$name, $args[$name], $type))
				throw new DatabaseException($conn);
			$i++;
		}
		
	}
	
	private static function getSQLString($class, array $params, $param_prefix = ":"){
		$names = array_keys($params);
		
		array_walk($names, function(&$item, $key) use ($param_prefix) {
			$item = $param_prefix.$item;	
		});
		
		return "call ".self::getProcedureName($class)."(".join(", ", $names).")";

	}
	
	
	private static function getParams($class){
		$obj = new \ReflectionClass($class);
		
		$params = $obj->getStaticPropertyValue(self::SATAIC_PARAMS, array());
		
		$pdo = new \ReflectionClass('\PDO');
		
		$result = [];
		$types = [];
		
		foreach ($params as $p){
			if (strpos($p, "::") === false){
				$name = $p;
				$types = null;
			} else 
				list($name, $types) = explode("::", $p, 2);
			
			if (strpos($p, "::") === false){
				$types = array();
			} else 
				$types = explode("::", $types);
			
			$pdo_type = 0;
			foreach ($types as $type){
			
				$type = strtoupper($type);
			
				if ($type == null || $type == "STRING")
					$type = "STR";
			
				if ($pdo->hasConstant("PARAM_".$type)){
					
					$pdo_type = $pdo_type | $pdo->getConstant("PARAM_".$type);
					
				} else if ($type == "INOUT") {
					
					$pdo_type = $pdo_type | \PDO::PARAM_INPUT_OUTPUT;
					
				} else
					throw new DatabaseException("Parametr type \"$type\" not found. Declared as \"$p\".");
			}
			
			$result[$name] = ($pdo_type == 0 ? \PDO::PARAM_STR : $pdo_type);
		}
		
		return $result;
	}
	
	
	private static function getProcedureName($class){
		$obj = new \ReflectionClass($class);
		
		$name = $obj->getShortName();
		
		if ($obj->hasConstant(self::CONST_PROC_NAME))
			$name = $obj->getConstant(self::CONST_PROC_NAME);
		
		return $name;
	}
	
	
}

