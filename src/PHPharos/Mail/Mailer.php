<?php
namespace PHPharos\Mail;


use PHPharos\Commons\Strings;
use PHPharos\Commons\File;
use PHPharos\Pharos;
use PHPharos\Exceptions\TemplateNotFoundException;
abstract class Mailer {

    private static $defaults = array(
        'charset' => 'utf-8',
        'method' => 'smtp',
        'smtp.secure' => '',
        'smtp.port' => 25,
    	'smtp.username' => null,
    	'smtp.password' => null,
    	'smtp.secure' => null,
    	'force_recipient' => null
    );

    private static $context = '';

    /** @var \PHPMailer */
    private static $mailer;

    /** @var array */
    private static $args = array();

    private function __construct(){}

    /**
     * @param $name
     * @param $value
     */
    protected static function put($name, $value){
        self::$args[$name] = $value;
    }

    /**
     * @param array $values
     */
    protected static function putAll(array $values){
         self::$args = array_merge($this->args, $values);
    }

    /**
     * @param string $subject
     * @return $this
     */
    protected static function setSubject($subject){
        self::getMailer()->Subject = $subject;
    }

    /**
     * @param $email
     * @param string $name
     * @return $this
     */
    protected static function addRecipient($email, $name = ''){
    	if (self::$defaults['force_recipient'] != null)
        	self::getMailer()->addAddress(self::$defaults['force_recipient'], $name);
    	else 
    		self::getMailer()->addAddress($email, $name);
    }

    /**
     * @param $email
     * @param string $name
     * @return $this
     */
    protected static function setFrom($email, $name = ''){
        self::getMailer()->setFrom($email, $name);
    }

    /**
     * @param File $file
     * @param string $name
     * @return $this
     */
    protected static function addAttachment(File $file, $name = ''){
        self::getMailer()->addAttachment($file->getPath(), $name);
    }

    /**
     * @param File $file
     * @param string $cid
     * @param string $name
     * @return $this
     */
    protected static function addEmbeddedImage(File $file, $cid, $name = ''){
        self::getMailer()->addEmbeddedImage($file->getPath(), $cid, $name);
    }

    /**
     * @param $email
     * @param string $name
     * @return $this
     */
    protected static function addReplyTo($email, $name = ''){
        self::getMailer()->AddReplyTo($email, $name);
    }

    /**
     * @param string $email
     * @param string $name
     * @return $this
     */
    protected static function addCC($email, $name = ''){
    	if (self::$defaults['force_recipient'] == null)
    		self::getMailer()->addCC($email, $name);
    }

    /**
     * @param string $email
     * @param string $name
     * @return $this
     */
    protected static function addBCC($email, $name = ''){
    	if (self::$defaults['force_recipient'] == null)
        	self::getMailer()->AddBCC($email, $name);
    }

    /**
     * @param string $charset
     * @return $this
     */
    protected static function setCharset($charset){
        self::getMailer()->CharSet = $charset;
    }

    /**
     * @param string $template
     * @param $result
     * @return void
     */
    private static function putTemplate($template, &$result, $ext = 'html'){
    	$template = Pharos::getCurrent()->loadTemplate((isset(self::$context) ? self::$context . '/' : '') . $template, $ext);

        $result = $template->fetch(self::$args);
    }

    /**
     * @param $template
     * @return void
     */
    private static function putHtmlTemplate($template){
        self::getMailer()->IsHTML(true);
        self::putTemplate($template, self::getMailer()->Body, 'html');

        try {
        	self::putTemplate($template, self::getMailer()->AltBody, 'txt');
        } catch (TemplateNotFoundException $e){
        	return;	
        }        
    }

    /**
     * @param string $template
     */
    private static function putTextTemplate($template){
        self::getMailer()->IsHTML(false);
        self::putTemplate($template, self::getMailer()->Body, 'txt');
    }

    /**
     * Send html email
     * @param bool|string $template
     * @return bool
     */
    protected static function send($template = false){
        if (!$template){
            $class = get_called_class();
            $class = str_replace('\\', '/', $class);

//             if (Strings::startsWith($class, 'Notifiers/')){
//                 $class = Strings::substring($class, 10);
//             }

            $callers  = debug_backtrace();
            $method   = $callers[1]['function'];
            $template = $class . '/' . $method;
        }

        self::putHtmlTemplate($template);
        $r = self::getMailer()->Send();
        
        self::$mailer = null;
        self::$args = array();
        
        return $r;
    }

    /**
     * @param bool|string $template
     * @return bool
     */
    protected static function sendText($template = false){
        if (!$template){
            $class = get_called_class();
            $class = str_replace('\\', '/', $class);

//             if (Strings::startsWith($class, 'Notifiers/')){
//                 $class = Strings::substring($class, 10);
//             }

            $callers  = debug_backtrace();
            $method   = $callers[1]['function'];
            $template = $class . '/' . $method;
        }

        self::putTextTemplate($template);
        $r = self::getMailer()->Send();
        
        self::$mailer = null;
        self::$args = array();
        
        return $r;
    }

    /**
     * @param string $context
     * @return $this
     */
    protected static function setContext($context){
        self::$context = $context;
    }

    private static function getMailer(){
        if (isset(self::$mailer)) 
        	return self::$mailer;

        $app = Pharos::getCurrent();
        
        if ($app){
            $config = $app->getConfig();

            self::$defaults = array(
                'method'  => $config->getString('mail.method', self::$defaults['method']),
                'charset' => $config->getString('mail.charset', self::$defaults['charset']),
                'smtp.host' => implode(';', $config->getArray('mail.smtp.host')),
                'smtp.port' => $config->getNumber('mail.smtp.port', self::$defaults['smtp.port']),
                'smtp.username' => $config->getString('mail.smtp.username', self::$defaults['smtp.username']),
                'smtp.password' => $config->getString('mail.smtp.password', self::$defaults['smtp.password']),
                'smtp.secure'  => $config->getString('mail.smtp.secure', self::$defaults['smtp.secure']),
            	'force_recipient'  => $config->getString('mail.force_recipient', self::$defaults['force_recipient'])            		
            );
        }
        
        self::$mailer = new \PHPMailer(true);
        
        switch(self::$defaults['method']){
        	case 'smtp': self::$mailer->IsSMTP(); break;
        	case 'sendmail': self::$mailer->IsSendmail(); break;
        	case 'mail': self::$mailer->IsMail(); break;
        	default: {
        		throw new \InvalidArgumentException(Strings::format('Unknown method for send email - "%s"', self::$defaults['method']));
        	}
        }
        
        self::$mailer->Host = self::$defaults['smtp.host'];
        self::$mailer->Port = self::$defaults['smtp.port'];
        self::$mailer->SMTPSecure = self::$defaults['smtp.secure'];
        
        
        if (self::$defaults['smtp.username'] && self::$defaults['method'] == 'smtp'){
        	self::$mailer->SMTPAuth = true;
        	self::$mailer->Username = self::$defaults['smtp.username'];
        	self::$mailer->Password = self::$defaults['smtp.password'];
        }
        
        self::setCharset(self::$defaults['charset']);
        
        return self::$mailer;
    }
}