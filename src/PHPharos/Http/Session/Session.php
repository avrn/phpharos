<?php
namespace PHPharos\Http\Session;


use PHPharos\Http\Session\Drivers\DefaultSessionDriver;

use PHPharos\Http\Session\Drivers\APCSessionDriver;

/**
 * Class Session
 */
class Session {

	protected static $instance;
	
    private $init = false;
    private $id;
    private $driver;

    protected function __construct(SessionDriver $driver){
        $this->driver = $driver;
        $this->check();
    }

    public function check(){
        if (!$this->init){
            $this->id = $this->driver->getSessionId();
            if (!$this->id)
                session_start();

            $this->init = true;
        }
    }

    public function isInit(){
        return $this->init;
    }

    /**
     * @return string
     */
    public function getId(){
        $this->check();
        return $this->driver->getSessionId();
    }

    /**
     * @return array
     */
    public function all(){
        $this->check();
        return (array)$_SESSION;
    }

    /**
     * @param string $name
     * @param mixed $def
     * @return null|scalar
     */
    public function get($name, $def = null){
        $this->check();
        return $this->has($name) ? $_SESSION[$name] : $def;
    }

    /**
     * @param $name string
     * @param $value string|int|float|null
     */
    public function put($name, $value){
        $this->check();
        $_SESSION[$name] = $value;
    }

    /**
     * @param array $values
     */
    public function putAll(array $values){
        $this->check();
        foreach($values as $name => $value){
            $this->put($name, $value);
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has($name){
        $this->check();
        return isset($_SESSION[$name]);
    }

    /**
     * @param string $name
     */
    public function remove($name){
        $this->check();
        $value = $_SESSION[$name];
        unset($_SESSION[$name]);
        return $value;
    }

    /**
     * clear all session values
     */
    public function clear(){
        $this->check();
        session_unset();
    }

    public static function getInstance() {
    	if (!isset(self::$instance)){
    		if (function_exists("apc_exists"))
    			$driver = APCSessionDriver::getInstance(); 
    		else 
    			$driver = DefaultSessionDriver::getInstance();
    		
    		$driver->register();
    		self::$instance = new Session($driver);
    	}
    	
        return self::$instance;
    }
}
