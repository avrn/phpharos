<?php
namespace PHPharos\Http\Session\Drivers;

use PHPharos\Http\Session\SessionDriver;

class DefaultSessionDriver extends SessionDriver {

    public function open($savePath, $sessionName) {
        // nop
    }

    public function close() {
        // nop
    }

    public function read($id) {
        // nop
    }

    public function write($id, $value) {
        // nop
    }

    public function destroy($id) {
        // nop
    }

    public function gc($lifetime) {
        // nop
    }

    public function register(){
        //parent::register();
    }
}
