<?php
namespace PHPharos\Http\Session;


abstract class SessionDriver {
	protected static $instance;
    
    abstract public function open($savePath, $sessionName);
    abstract public function close();
    abstract public function read($id);
    abstract public function write($id, $value);
    abstract public function destroy($id);
    abstract public function gc($lifetime);

    public function getSessionId(){
        return session_id();
    }

    public function register(){
        session_set_save_handler(
            array($this, 'open'), array($this, 'close'),
            array($this, 'read'), array($this, 'write'),
            array($this, 'destroy'), array($this, 'gc')
        );

    }

    public static function getInstance() {
        if (!isset(self::$instance)){
        	$class = get_called_class();
        	self::$instance = new $class();
        }
        
        return self::$instance;
    }
}