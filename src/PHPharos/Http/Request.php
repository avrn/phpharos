<?php

namespace PHPharos\Http;


use PHPharos\Commons\Strings;
use PHPharos\Http\URL;
use PHPharos\Pharos;

class Request {
	private static $instance;

    protected $method = "GET";
    protected $uri    = "";
    protected $host   = "";
    protected $userAgent = "";
    protected $referer   = "";
    protected $port      = "";
    protected $protocol  = "http";
    protected $ipAddress = "";
    
    protected $basePath = "";

    /**
     * @var URL
     */
    protected $currentUrl;

    /**
     * @var array
     */
    protected $headers;

    private function __construct($headers = null) {
        $this->headers = $headers;
    }
    
    /**
     * @return \PHPharos\Http\Request
     */
    public static function getInstance(){
    	if (!self::$instance)
    		self::$instance = self::createFromGlobal();
    	
    	return self::$instance;
    }

    private static function createFromGlobal($headers = null){
        if ($headers === null)
            if ( function_exists('getallheaders') ){
                $headers = getallheaders();
            } else if ( function_exists('apache_request_headers') ){
                $headers = apache_request_headers();
            } else {
                foreach($_SERVER as $key=>$value) {
                    if (substr($key,0,5)=="HTTP_") {
                        $key = str_replace(" ", "-", str_replace("_"," ",substr($key,5)));
                        $headers[$key] = $value;
                    }
                }
            }
        $headers = array_change_key_case((array)$headers, CASE_LOWER);

        $req = new Request($headers);
        $req->setMethod($_SERVER['REQUEST_METHOD']);
        $req->setUri(isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null);
        $req->userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        $req->referer   = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
        $req->protocol  = isset($_SERVER['HTTPS']) ? 'https' : 'http';
		$req->ipAddress = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
		
        $host = explode(':', $_SERVER['HTTP_HOST']);
        $req->host = $host[0];
        $req->port = isset($host[1]) ? (int)$host[1] : ($req->protocol == 'https' ? 443 : 80);

        $req->currentUrl = URL::buildFromUri( $req->host, $req->uri, $req->protocol, $req->port );
        return $req;
    }
    
    /**
     * @return boolean
     */
    public function isRobot(){
    	return Strings::contains(strtolower($this->userAgent), "robot") ||
    			Strings::contains(strtolower($this->userAgent), "bot");
    }

    /**
     * @return string calc hash of request
     */
    public function getHash(){
        return sha1(
            $this->method. '|' .
                $this->protocol . '|' .
                $this->host . '|' .
                $this->port . '|' . $this->uri );
    }

    /**
     * @return string
     */
    public function getUri(){
        return $this->uri;
    }

    /**
     * @param $name
     * @param null $def
     * @return array|null
     */
    public function getHeader($name, $def = null){
        $name = strtolower($name);
        return isset($this->headers[$name]) ? $this->headers[$name] : $def;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasHeader($name){
        return isset($this->headers[strtolower($name)]);
    }

    /**
     * query from request uri
     * @return string
     */
    public function getQuery(){
        $tmp = explode('?', $this->uri, 2);
        return (string)$tmp[1];
    }

    /**
     * get request path
     * @return string
     */
    public function getPath(){
        $tmp = explode('?', $this->uri, 2);
        return (string)$tmp[0];
    }

    /**
     * @param string $url
     */
    public function setUri($url){
        $this->uri = $url;

        if ( $this->basePath ){
            $p = strpos($this->uri, $this->basePath);
            if ( $p === 0 )
                $this->uri = substr($this->uri, strlen($this->basePath));

            if ( !$this->uri )
                $this->uri = '/';
            else if ($this->uri[0] !== '/')
                $this->uri = '/' . $this->uri;
        }
    }

    /**
     * @return string
     */
    public function getHost(){
        return $this->host;
    }

    /**
     * @return string
     */
    public function getUserAgent(){
        return $this->userAgent;
    }

    /**
     * @return string
     */
    public function getReferer(){
        return $this->referer;
    }

    public function refererInRules(){
    	$rules = Pharos::getCurrent()->getRules();
    	
    	foreach ($rules as $r)
    		if (Strings::contains($this->getReferer(), $r->getHost()))
    			return true;
    		
    	return false;
    }
    
    /**
     * @return string
     */
    public function getMethod(){
        return $this->method;
    }
    
    /**
     * @return string
     */
    public function getIpAddress(){
    	return $this->ipAddress;
    }

    /**
     * get languages from accept-languages
     * @return array
     */
    public function getLanguages(){
        $langs = array();
        $info  = $this->headers['accept-language'];
        $types = explode(';', $info, 10);
        foreach($types as $type){
            $meta = explode(',', $type);
            if ( $meta[1] )
                $lang = $meta[1];
            elseif ( $meta[0] && substr($meta, 0, 2) != 'q=' )
                $lang = $meta[0];

            if ( strpos('-', $lang) !== false ){
                $lang = explode('-', $lang);
                $lang = $lang[0];
            }
            $langs[] = trim($lang);
        }
        return $langs;
    }

    /**
     * @param string $method - get, post, etc.
     * @return \regenix\mvc\Request
     */
    public function setMethod($method){
        $this->method = strtoupper($method);
        return $this;
    }

    public function setBasePath($path){
        $this->basePath = $path;
        $this->setUri($this->getUri());

        return $this;
    }

    /**
     * @param int $port
     */
    public function setPort($port){
        $this->port = $port;
        $this->currentUrl->setPort($port);
    }

    /**
     * @return boolean
     */
    public function isPost(){
        return $this->isMethod('POST');
    }

    /**
     * @return bool
     */
    public function isGet(){
        return $this->isMethod('GET');
    }

    /**
     * @param string $method - post, get, put, delete, etc.
     * @return boolean
     */
    public function isMethod($method){
        return $this->method === strtoupper($method);
    }

    /**
     *
     * @param array $methods
     * @return boolean
     */
    public function isMethods(array $methods){
        foreach ($methods as $value) {
            if ( strtoupper($value) === $this->method )
                return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isAjax(){
        return strtolower($this->getHeader('x-requested-with')) === 'xmlhttprequest';
    }


    /**
     * @param string $etag
     * @return bool
     */
    public function isCachedEtag($etag){
        $tagHead = 'if-none-match';
        if ($this->hasHeader($tagHead)){
            $rTag = $this->getHeader($tagHead);
            return $rTag === $etag;
        } else
            return false;
    }

    /**
     * @param $baseUrl
     * @internal param \regenix\mvc\URL|string $url
     * @return boolean
     */
    public function isBase($baseUrl){
        if ( !($baseUrl instanceof URL) ){
            $baseUrl = new URL($baseUrl);
        }
        return $this->currentUrl->constraints($baseUrl);
    }

    /**
     * @param string $domain
     * @param bool $cutWWW
     * @return bool
     */
    public function isDomain($domain, $cutWWW = true){
        $host = $this->currentUrl->getHost();
        if ($cutWWW){
            if (Strings::startsWith($host, 'www.'))
                $host = substr($host, 4);

            if (Strings::startsWith($domain, 'www.'))
                $domain = substr($domain, 4);
        }
        return (strtolower($host) === strtolower($domain));
    }

}
