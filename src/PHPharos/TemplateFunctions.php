<?php

namespace PHPharos;

use PHPharos\Routing\Router;
class TemplateFunctions {
	
	public static function resource($uri){
		return str_replace("//" ,"/", "/apps/".Pharos::getCurrent()->getName()."/public/".$uri);
		//return str_replace("//" ,"/", "/public/".$uri);
	}
	
	
	public static function replace($variable, $search, $replace) {
		return str_replace($search, $replace, $variable.'');
	}	
	
	
	public static function text($variable) {
		return strip_tags($variable);
	}
	
	
	public static function rub($variable) {
		return number_format($variable, 0, '.', ' ').' руб.';
	}
	
	
	public static function url($variable, $params) {
		$p = array();

		if (isset($params) && count($params) > 0)
			foreach ($params as $name=>$value){
				if (isset($value) && $value != '')
					$p[] = $name.'='.$value;
			}
		
		if (count($p) > 0 )
			return $variable.'?'.join("&", $p);
		
		return $variable;
	}
	
	
	public static function route($action = null, $args = array(), $withArgs = false) {
		$url = Router::path($action, $withArgs == true ? null : $args);
	
		return $url;
	}
}
