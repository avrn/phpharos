<?php
namespace PHPharos\Validation\Exceptions;

use PHPharos\Commons\Strings;

class ValidationException extends \LogicException {
	
	public function __construct($message) {
		$args = array();
		if (func_num_args() > 1)
			$args = array_slice(func_get_args(), 1);
	
		parent::__construct(Strings::formatArgs($message, $args));
	}
	
}
