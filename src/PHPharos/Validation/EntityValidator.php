<?php
namespace PHPharos\Validation;

use PHPharos\Validation\Results\ValidationResult;
use PHPharos\Validation\Results\ValidationIsEmptyResult;
use PHPharos\Validation\Results\ValidationRequiresResult;
use PHPharos\Validation\Results\ValidationMinLengthResult;
use PHPharos\Validation\Results\ValidationMaxLengthResult;
use PHPharos\Validation\Results\ValidationFileMaxSizeResult;
use PHPharos\Validation\Results\ValidationFileTypeResult;
use PHPharos\Validation\Results\ValidationMatchesResult;
use PHPharos\Validation\Results\ValidationFilterResult;
use PHPharos\Validation\Results\ValidationCallbackResult;
use PHPharos\Validation\Results\ValidationEqualsResult;
use PHPharos\Validation\Results\ValidationLengthResult;

class EntityValidator extends AbstractValidator {

    /** @var mixed */
    protected $entity;

    public function __construct($entity){
        $this->entity = $entity;
//         $this->entityName = isset($entityName) ? Strings::replace('[]', '',  $entityName) : null;
    }

    /**
     * 
     * @return \PHPharos\Validation\EntityValidator
     */
    protected function validateAttr($attribute, $message, $title, ValidationResult $validation){
        return $this->validate(
        		$this->entity,
        		$validation,
        		$attribute, 
        		$title,
        		$message
        	);
    }

    public function addError($attribute, $message, $title = null){
    	$validation = new ValidationCallbackResult(function(){return false;});
       	return $this->validateAttr($attribute, $message, $title, $validation);
    }

    public function isEmpty($attribute, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationIsEmptyResult());
    }

    public function requires($attribute, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationRequiresResult());
    }

    public function length($attribute, $length, $title = null, $message = null){
    	return $this->validateAttr($attribute, $message, $title, new ValidationLengthResult($length));
    }
    
    public function minLength($attribute, $min, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationMinLengthResult($min));
    }

    public function maxLength($attribute, $max, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationMaxLengthResult($max));
    }
    
    public function equals($attribute, $sample, $title = null, $message = null){
    	return $this->validateAttr($attribute, $message, $title, new ValidationEqualsResult($sample));
    }

    public function maxFileSize($attribute, $size, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationFileMaxSizeResult($size));
    }

    public function isFileType($attribute, array $extensions, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationFileTypeResult($extensions));
    }

    public function matches($attribute, $pattern, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationMatchesResult($pattern));
    }

    public function checkFilter($attribute, $filter, $title = null, $message = null){
        return $this->validateAttr($attribute, $message, $title, new ValidationFilterResult($filter));
    }
    
    public function email($attribute, $title = null, $message = 'validation.result.email'){
    	return $this->checkFilter($attribute, FILTER_VALIDATE_EMAIL, $title, $message);
    }
    
}
