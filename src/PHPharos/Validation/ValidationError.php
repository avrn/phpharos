<?php 

namespace PHPharos\Validation;

class ValidationError{
	public $attribute;
	public $message;
	public $value; 
	public $title;
	
	public function __construct($message, $attribute, $value){
		$this->attribute = $attribute;
		$this->message = $message;
		$this->value = $value;
	}
	
	
	public function getAttribute($entityName = null){
		if($entityName == null)
			return $this->attribute;
		 
		if (is_array($this->value))
			return $entityName . '[' . $this->attribute . ']';
		
		if (is_object($this->value))
			return $entityName . '.' . $this->attribute;
		
		return $this->attribute;
	}
}