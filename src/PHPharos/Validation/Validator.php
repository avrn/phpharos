<?php
namespace PHPharos\Validation;


interface Validator {
	/**
	 * @return \PHPharos\Validation\AbstractValidator
	 */	
	public function validator();
	
}
