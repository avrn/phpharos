<?php

namespace PHPharos\Validation;

use PHPharos\Http\Session\Flash;
use PHPharos\Commons\Strings;

class Validation {
	const FLASH_PREFIX = '$$validation';
	
	/** @var array */
	protected $errors;
	
	private function __construct(){
		$flash = Flash::getInstance();
		
		if (!isset($this->errors)){
			$this->errors = $flash->get('errors'.self::FLASH_PREFIX, array());
		}
	}
	
	public function keep(){
		$flash = Flash::getInstance();
		$flash->put('errors'.self::FLASH_PREFIX, $this->errors);
		return $this;
	}
	
	public function addError($attribute, $message){
		$this->errors[] = array('attribute' => $attribute, 'message' =>$message);
	}
	
	public function clear(){
		$this->errors = array();
	}
	
	/**
	 * @return bool
	 */
	public function hasError($attr){
		$attr = trim($attr, "[]");
		
  		foreach ($this->errors as $e)
			//if ($e['attribute'] == $attr)
  			if (Strings::contains($e['attribute'], $attr))
				return true;
		return false;
	}
	
	/**
	 * @return bool
	 */
	public function hasErrors(){
		return sizeof($this->errors) > 0;
	}
	
	public function error($attr){
		$r = array();
		$attr = trim($attr, "[]");
		
		foreach ($this->errors as $e)
			if ($e['attribute'] == $attr){
				$r[] = $e['message'];
			}
		return implode(', ', $r);
	}
	
	/**
	 * @return array
	 */
	public function getErrors(){
		return $this->errors;
	}
	
	
// 	/**
// 	 * @return \PHPharos\Validation\Validator
// 	 */	
// 	public function variable(){
// 		return new Validator();
// 	}
	
	/**
	 * @param object|array $entity
	 * @param string $entityName
	 * @return \PHPharos\Validation\EntityValidator
	 */
	public function entity($entity){
		return new EntityValidator($entity);
	}
	
	
	protected static $instance;
	public static function getInstance(){
		if (!isset(self::$instance)){
    		self::$instance = new Validation();
		}
		
        return self::$instance;
	}
}

