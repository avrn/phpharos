<?php
namespace PHPharos\Validation;

use PHPharos\Validation\Results\ValidationResult;
use PHPharos\Validation\Exceptions\ValidationException;

class AbstractValidator {
	
	/** @var array */
	protected $errors;
	
	/** @var boolean */
	protected $__ok = true;
	
	/** @var bool */
	protected $__lastOk;
		
	public function isOk(){
		return $this->__ok;
	}
	
	public function isLastOk(){
		return $this->__lastOk;
	}
	
	public function clear(){
		$this->errors = array();
	}
	
	public function getErrors(){
		return $this->errors;
	}
	
	/**
	 * @return bool
	 */
	public function hasErrors(){
		return sizeof($this->errors) > 0;
	}
	
	public function getErrorsAsString(){
		$errors = array();
		foreach($this->errors as $e){
			$errors[] = $e->attribute.": ".$e->message;	
		}
		
		return join("; ", $errors);
	}

	/**
	 * get value by name of attribute
	 * example: getValue('name'), getValue('address.name')
	 * @param $attribute
	 * @throws ValidationException
	 * @return mixed|null
	 */
	protected function getValue($entity, $attribute = null){
		if (!isset($attribute))
			return $entity;
		
		$obj   = $entity;
		$value = null;
	
		if (is_object($obj)){
			if (property_exists($obj, $attribute))
				$value = $obj->{$attribute};
			else
				throw new ValidationException('`%s` attribute does not exist in the %s class', $attribute, get_class($this->entity));
		} else if (is_array($obj)){
			if (isset($obj[$attribute]))
				$value = $obj[$attribute];
			else
				throw new ValidationException('`%s` key not exists in array', $attribute);
		} else
			throw new ValidationException('`%s` attribute must be an object or array', $attribute);
	
		return $value;
	}
	
    /**
     * @param $value
     * @param $message
     * @param ValidationResult $validation
     * @return ValidationResult
     */
	protected function validate($entity, ValidationResult $validation, $attribute = null, $title = null, $message = null){

        $this->__lastOk = true;
        $value = $this->getValue($entity, $attribute);
        
        if (!$validation->getResult($value)){
        	$validation->setMessage($message);
        	
        	$e = new ValidationError(
				            		$validation->getMessage($attribute, $title), 
				            		$attribute, 
				            		$entity
				            	);
        	
            $this->errors[] = $e; 
            $this->__ok = false;
            $this->__lastOk = false;
        }
        
        return $this;
    }
}
