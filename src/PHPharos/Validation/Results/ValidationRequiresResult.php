<?php
namespace PHPharos\Validation\Results;

class ValidationRequiresResult extends ValidationResult {
	
	const KEY = 'validation.result.requires';

    public function check($value){
        return isset($value) && strlen(trim($value)) > 0;
    }
}
