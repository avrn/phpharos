<?php
namespace PHPharos\Validation\Results;

class ValidationLengthResult extends ValidationResult {
	
	const KEY = 'validation.result.length';

    private $length;

    public function __construct($length){
        $this->length = (int)$length;
    }

    public function check($value){
    	$l = mb_strlen((string)$value, 'utf-8');
        return  $l == $this->length;
    }

    public function getMessageAttr(){
        return array('length' => $this->length);
    }
}
