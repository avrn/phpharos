<?php
namespace PHPharos\Validation\Results;

class ValidationMinLengthResult extends ValidationResult {

	const KEY = 'validation.result.minLength';

    private $min;

    public function __construct($min){
        $this->min = (int)$min;
    }

    public function check($value){
        return strlen((string)$value) >= $this->min;
    }

    public function getMessageAttr(){
        return array('min' => $this->min);
    }
}
