<?php
namespace PHPharos\Validation\Results;


use PHPharos\i18n\I18n;
abstract class ValidationResult {

	const KEY = 'validation.result.default';
	
    /** @var bool */
    private $ok = false;

    /** @var string */
    private $message = null;

    
    public function setMessage($message){
        $this->message = $message;
        return $this;
    }

    /**
     * @param $value
     * @return bool
     */
    public function getResult($value){
        $result = $this->check($value);
        return $this->ok = !!$result;
    }

    /**
     * returns true if validation is ok
     * @return bool
     */
    public function isOk(){
        return $this->ok;
    }

    /**
     * returns additional message args
     * @return array
     */
    protected function getMessageAttr(){
        return array();
    }

    /**
     * main method for checking value
     * @param $value
     * @return mixed
     */
    abstract public function check($value);

    /**
     * return an end formatted message of validation
     * @param $attr
     * @return string
     */
    public function getMessage($attr, $title = null){
        return I18n::get(
            isset($this->message) ? $this->message : static::KEY,
            array_merge(
                array(
                	'attribute' => $attr,
                	'value' => isset($title) ? $title : $attr	
        		),
                $this->getMessageAttr()
            )
        );
    }
}
