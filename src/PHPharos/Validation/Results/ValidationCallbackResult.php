<?php
namespace PHPharos\Validation\Results;

use PHPharos\Commons\Strings;

class ValidationCallbackResult extends ValidationResult {
 	
    /** @var callable */
    private $callback;

    public function __construct($callback){
        if (!is_callable($callback))
             throw new \RuntimeException(Strings::format('Argument "%s" must be `%s`', '$callback', 'callable'));

        $this->callback = $callback;
    }

    public function check($value){
        return call_user_func($this->callback, $value);
    }
}
