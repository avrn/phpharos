<?php
namespace PHPharos\Validation\Results;

class ValidationEqualsResult extends ValidationResult {
	
	const KEY = 'validation.result.equals';

    private $sample;

    public function __construct($sample){
        $this->sample = $sample;
    }

    public function check($value){
        return  $value == $this->sample;
    }

    public function getMessageAttr(){
        return array('sample' => $this->sample);
    }
}
