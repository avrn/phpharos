<?php
namespace PHPharos\Validation\Results;

use PHPharos\Commons\File;

class ValidationFileMaxSizeResult extends ValidationResult {

	const KEY = 'validation.result.maxFileSize';
	
    private $size;

    public function __construct($size){
        $this->size = $size;
    }

    public function check($value){
        $file = $value;
        if (!($file instanceof File))
            throw new \RuntimeException('Value of `FileMaxSize` validator must be an instance of File class');

        return $file->length() <= $this->size;
    }

    public function getMessageAttr(){
        return array('param' => $this->size);
    }
}
