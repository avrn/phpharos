<?php
namespace PHPharos\Validation\Results;

class ValidationMaxLengthResult extends ValidationResult {
	
	const KEY = 'validation.result.maxLength';

    private $max;

    public function __construct($max){
        $this->max = (int)$max;
    }

    public function check($value){
    	$l = mb_strlen((string)$value, 'utf-8');
        return  $l <= $this->max;
    }

    public function getMessageAttr(){
        return array('param' => $this->max);
    }
}
