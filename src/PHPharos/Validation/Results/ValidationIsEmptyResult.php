<?php
namespace PHPharos\Validation\Results;

class ValidationIsEmptyResult extends ValidationResult {
	
	const KEY = 'validation.result.isEmpty';
	
    public function check($value){
        return !$value;
    }
}
