<?php
namespace PHPharos\Validation\Results;

class ValidationMatchesResult extends ValidationResult {
	
	const KEY = 'validation.result.matches';
	
    private $pattern;

    public function __construct($pattern){
        $this->pattern = $pattern;
    }

    public function check($value){
        return preg_match($this->pattern, $value);
    }

    public function getMessageAttr(){
        return array('pattern' => $this->pattern);
    }
}
