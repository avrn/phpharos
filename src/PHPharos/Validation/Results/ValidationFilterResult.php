<?php
namespace PHPharos\Validation\Results;

class ValidationFilterResult extends ValidationResult {
	
	const KEY = 'validation.result.filter';
	
	
    private $filter;

    public function __construct($filter){
        $this->filter = $filter;
    }

    public function check($value){
        return filter_var($value, $this->filter) !== false;
    }
}
