<?php
namespace PHPharos\Validation\Results;

use PHPharos\Commons\File;
use PHPharos\Http\UploadFile;

class ValidationFileTypeResult extends ValidationResult {
	
	const KEY = 'validation.result.isFileType';
	
    private $types;

    public function __construct(array $types){
        $this->types = array_map('strtolower', $types);
    }

    public function check($value){
        $file = $value;
        if (!($file instanceof File))
            throw new \RuntimeException('Value of `FileMaxSize` validator must be an instance of File class');

        $ext = strtolower($file->getExtension());
        if ($file instanceof UploadFile){
            $ext = strtolower($file->getMimeExtension());
        }

        return in_array($ext, $this->types, true);
    }

    public function getMessageAttr(){
        return array('param' => implode(', ', $this->types));
    }
}
