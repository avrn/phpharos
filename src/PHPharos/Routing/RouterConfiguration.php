<?php

namespace PHPharos\Routing;

use PHPharos\Commons\Strings;
use PHPharos\Commons\File;
use PHPharos\Config\Configuration;


class RouterConfiguration extends Configuration {

    const type = __CLASS__;

    private static $routePattern = '#^(GET|POST|PUT|PATCH|DELETE|OPTIONS|HEAD|\*)[(]?([^)]*)(\))?\s+(.*/[^\s]*)\s+([^\s(]+)(.+)?(\s*)$#';

    /** @var array */
    private $patterns = array();

    /** @var File */


    public function addPattern($code, $routes){
        $this->patterns[ $code ] = $routes;
    }

    public function addPatterns(array $patterns){
        if (REGENIX_IS_DEV){
            foreach($patterns as $code => $routes)
                $this->addPattern($code, $routes);
        } else {
            $this->patterns = array_merge($this->patterns, $patterns);
        }
    }

    public function setPatternDir(File $dir){
        $this->patternDir = $dir;
    }

    public function validate(){
        foreach($this->data as $route){
            $action = $route['action'];
            $class  = substr($action, 0, strrpos($action, '.'));
            $method = substr($action, strrpos($action, '.') + 1);

            if (strpos($class, '{') === false && strpos($class, '}') === false){
                if (!class_exists(str_replace('.', '\\', $class))){
                    throw new \RuntimeException('The action "'.$class . '.*'.'" does not exist');
                }

                if (strpos($method, '{') === false && strpos($method, '}') === false){
                    if(!method_exists(str_replace('.', '\\', $class), $method)){
                        throw new \RuntimeException('The action "'.$class . '.' . $method . '()'.'" does not exist');
                    }
                }
            }
        }
    }

    public function loadData(){
        $files = $this->files;
        if ( !$files )
            $files = array($this->file);

        foreach($files as $prefix => $file){
            if (!$file->exists()) continue;

            $file->open('r');
            while (($buffer = $file->gets()) !== false) {
                $buffer = trim($buffer);
                if ( !$buffer || $buffer[0] == '#' )
                    continue;

                $matches = array();
                preg_match_all(self::$routePattern, $buffer, $matches);

                $method  = $matches[1][0];
                $headers = $matches[2][0];
                $path    = $matches[4][0];
                $action  = $matches[5][0];
                $params  = $matches[6][0];

                $params = substr(trim($params), 1, -1);
                $params   = explode(',', $params);
                $args = array();
                foreach($params as $el){
                	if (strlen($el) == 0)
                		continue;
                	
                    list($key, $val) = explode(':', $el, 2);
                    $args[trim($key)] = trim($val);
                }
                $params = $args;
                if (Strings::startsWith($action, 'static::')){
                	$this->data[] = array(
                			'method'  => $method,
                			'headers' => $headers,
                			'path'    => $path,
                			'action'  => 'PHPharos.StaticController.load',
                			'params'  => $params,
                			'static'  => Strings::substring($action, 8)
                	);
                	continue;
                	
                } else if (strpos($action, ':') !== false ){

                    $tmp = explode(':', $action, 2);
                    $pattern = $tmp[0];
                    $base    = trim($tmp[1]);

                    $patternRoutes = $this->patterns[$pattern];
                    if (!$patternRoutes){
                        $patternConfig = new RouterConfiguration();

                        $patternConfig->setFile(new File(str_replace('.', '/', $pattern) . '.route', $this->patternDir));
                        $patternConfig->setPatternDir($this->patternDir);
                        $patternConfig->load();

                        $patternRoutes = ($this->patterns[$pattern] = $patternConfig->getRouters());
                    }

                    if ($patternRoutes){
                        if ($base[0] != '.')
                            $base = '.controllers.' . $base;

                        $_keys = array_map(function($value){
                            return '[' . $value . ']';
                        }, array_keys($params));

                        $_keys[] = '[parent]';

                        foreach($patternRoutes as $el){
                            $relative = $el['path'][0] === '/';

                            if ($base !== '*' && $base !== '.controllers.*')
                                $el['action'] = $base . $el['action'];
                            else {
                                if ($relative)
                                    $el['path'] = substr($el['path'],1);
                            }

                            if ($relative){
                                $el['path'] = $path . $el['path'];
                            } else {
                                $el['path'] = '/' . str_replace(
                                    $_keys, array_merge($params, array('parent' => $path)), $el['path']
                                );
                            }

                            $this->data[] = $el;
                        }

                        continue;
                    } else
                        throw new \RuntimeException('Cannot find `'.$pattern.'` route pattern');
                }
                
                if (is_numeric($prefix)){
                    if ($action[0] != '.')
                        $action = '.Controllers.' . $action;
                } else {
                    $action = $prefix . $action;
                }
                
                $this->data[] = array(
                    'method'  => $method, 
                    'headers' => $headers,
                    'path'    => $path,
                    'action'  => $action,
                    'params'  => $params,
                	'static'  => null
                );
            }
            $file->close();
        }
    }
    
    public function getRouters(){
        return $this->data;
    }
}