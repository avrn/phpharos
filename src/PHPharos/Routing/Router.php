<?php

namespace PHPharos\Routing;


use PHPharos\Controller;

use PHPharos\Http\Request;
use PHPharos\Pharos;

class Router {
  

    /**
     * {
     *   [method] => POST | GET | DELETE | etc.
     *   [pattern] = /users/(.+)/ for /users/{id}/
     * }
     * @var array
     */
    private $routes = array();

    /** @var array */
    public $args = array();
    
    /** @var array */
    public $params = array();

    /** @var array route info */
    public $current = null;
    
    /** @var string */
    public $action;

    /** @var Request */
    private $request;

    public function __construct() {
        $this->request = Request::getInstance();
    }
    
    public function applyConfig(RouterConfiguration $config){
        foreach($config->getRouters() as $info){
            $this->addRoute($info['method'], $info['path'], str_replace('\\', '.', $info['action']), $info['static'], $info['params']);
        }
    }
    
    private function buildRoute($method, $path, $action, $static, $params = ''){
        $_path = str_replace(array('{', '}'), array('{#', '{'), $path);
        $_args = explode('{', $_path);
        
        // /users/{id}/{module}/

        $args    = array();
        $pattern = '';
        $types   = array();
        $patterns = array();
        $tmpPattern = '';
        $url     = '';
        foreach($_args as $i => $arg){
            if (strlen($arg) > 0 && $arg[0] == '#' ){
                if (($p = strpos($arg, '<')) !== false ){
                    $name  = substr($arg, 1, $p - 1);
                    $pattern .= ($tmpPattern = '(' . substr($arg, $p + 1, strpos($arg, '>') - $p - 1) . ')');
                } else {
                    $name  = substr($arg, 1);
                    $pattern .= ($tmpPattern = '([^/]+)');
                }

                if ( strpos($name, ':') === false ){
                    $args[] = $name;
                    $types[$name] = 'auto';
                } else {
                    $name = explode(':', $name, 3);

                    $types[$name[0]] = $name[1];
                    $args[] = $name = $name[0];
                }
                $url .= '{' . $name . '}';
                $patterns[$name] = $tmpPattern;
            } else {
                $url     .= $arg;
                $pattern .= $arg;
            }
        }
		
        $app  = Pharos::getCurrent();
        $rule_path = $app ? $app->getCurrentRule()->getPath() : '';
        
        $item = array(
                'method'  => strtoupper($method),
                'path'    => $path,
                'action'  => str_replace('\\', '.', $action),
                'params'  => $params,
                'types'   => $types,
                'pattern' => '#^' . ($rule_path === '/' ? '' : $rule_path). $pattern . '$#',
                'args'    => $args,
                'patterns' => $patterns,
                'url'     => $url,
        		'static'  => $static
        );

        return $item;
    }

    /**
     * @param $action
     * @param array $args
     * @param string $method
     * @return mixed|null|string
     */
    public function reverse($action, array $args = array(), $method = '*'){
        $defAction = $action;
        $originalAction = $action;

        if ($action !== null){
            $action = str_replace('\\', '.', $action);

            if ($action[0] != '.')
                $action = '.controllers.' . $action;

            $originalAction = $action;
            $action = strtolower($action);
        }

        $originArgs = $args;
        foreach($this->routes as $route){
        	$args = $originArgs;
            if ($method != '*' && $route['method'] != '*' && strtoupper($method) != $route['method'])
                continue;

            $replace = array('_METHOD');
            $to      = array($method == '*' ? 'GET' : strtoupper($method));
            $routeKeys = array_keys($route['types']);
			$routeArgs = array();
			
            if ($action){
                $cur = preg_quote($route['action'], '#');

                
                foreach($route['patterns'] as $param => $regex){
                    $cur = str_replace('\{' . $param . '\}', $regex, $cur, $count);
                    if ($count < 1)
                    	foreach ($routeKeys as $i => $key)
                    		if ($key == $param)
                    			unset($routeKeys[$i]); 
                }
                $routeKeys = array_values($routeKeys);
                
                // search args in route address
                preg_match_all('#^' . $cur . '$#i', $originalAction, $matches);
                foreach($matches as $i => $value){
                    if ($i){
                        $routeArgs[$routeKeys[$i-1]] = current($value);
                    }
                }
                
                $args = array_merge($routeArgs, $args);

                foreach($args as $key => $value){
                    if (isset($route['types'][$key])){
                        $replace[] = '{' . $key . '}';
                        $to[]      = $value;
                    }
                }
                $curAction = str_replace($replace, $to, $route['action']);
            }

            if ( $defAction === null || $action === strtolower(str_replace('\\', '.', $curAction)) ){
                $match = true;

                if ($action)
                foreach($route['patterns'] as $name => $pattern){
                    if (!isset($args[$name]) || !preg_match('#^'. $pattern . '$#', (string)$args[$name])){
                        $match = false;
                        break;
                    }
                }

                if ($match){
                    $url = str_replace($replace, $to, $action === null ? '' : $route['url']);
                    $i = 0;
                    foreach($args as $key => $value){
                    	if (!empty($value))
                        if (!isset($route['types'][$key]) && isset($value)){
                            $url .= ($i == 0 ? '?' : '&');
                            if (is_array($value)){
                                $kk = 0;
                                foreach($value as $k => $el){
                                    $kk += 1;
                                    $url .= $key . '[' . $k . ']=' . urlencode($el) . ($kk < sizeof($value) ? '&' : '');
                                }
                            } else {
                                $url .= $key . '=' . urlencode($value);
                            }
                            $i++;
                        }
                    }

                    if (!$action)
                        return $url;

                    $app  = Pharos::getCurrent();
                    $path = $app ? $app->getCurrentRule()->getPath() : '';

                    return ($path === '/' ? '' : $path) . strtolower($url);
                }
            }
        }
        return null;
    }
    
    public function addRoute($method, $path, $action, $static, $params = ''){
        $this->routes[] = $this->buildRoute($method, $path, $action, $static, $params);
    }

    public function invokeMethod(Controller $controller, \ReflectionMethod $method){
    	$params = $method->getParameters();
    	$args = array();
    		
    	/* @var $p \ReflectionParameter */
    	foreach($params as $p){
    		$paramName = $p->getName();
			
    		/* @var $class \ReflectionClass */
    		$class = $p->getClass();
    		if ($class != null){
    		    			
    			$instance = $class->newInstanceWithoutConstructor();
    			$props = $class->getProperties();
    			$is_set = false;
    			/* @var $prop \ReflectionProperty */
    			foreach($props as $prop){
    				if ($prop->isPrivate() || $prop->isProtected()){
    					$prop->setAccessible(true);
    				}
    	
    				$request_name = $paramName."_".$prop->getName();
    				$request_value = isset($_REQUEST[$request_name]) ? $_REQUEST[$request_name] : null;
    				if ($request_value != null){
    					$prop->setValue($instance, $request_value);
    					$is_set = true;
    				}
    			}
    			
    			$args[$p->getPosition()] = !$p->isDefaultValueAvailable() || $is_set ? $instance : null;
    		} else
    			if ( isset($this->args[$paramName]) ){
    				$args[$p->getPosition()] = $this->args[$paramName];
    			} else if ( isset($this->params[$paramName]) ){
    				$args[$p->getPosition()] = $this->params[$paramName];
    			} else if (isset($_REQUEST[$paramName])){
    				$args[$p->getPosition()] = $_REQUEST[$paramName];
    			} else {
    				$args[$p->getPosition()] = ($p->isDefaultValueAvailable() ? $p->getDefaultValue() : null);
    			}
    	}
    	
    	return $method->invokeArgs($controller, $args);
    }

    public function route(){
        $request = $this->request;

        $isCached = false;
        if ($isCached){
            $hash  = $request->getHash();
            //$datas = SystemCache::get('routes');

            if ( $datas === null )
                $datas = array();

            $data = $datas[ $hash ];

            if ( $data !== null ){
                $this->args   = $data['args'];
                $this->params = $data['params'];
                $this->action = $data['action'];
                return;
            }
        }
        
        $method = $request->getMethod();
        $path   = $request->getPath();
        $format = '';
        $domain = $request->getHost();

        foreach($this->routes as $route){
            $args = self::routeMatches($route, $method, $path, $format, $domain);

            if ( $args !== null ){
                $this->args    = $args;
                $this->current = $route;
                $this->params = $route['params'];
                $this->action  = $route['action'];
                if (strpos($this->action, '{') !== false){
                    foreach ($args as $key => $value){
                        $this->action = str_replace('{' . $key . '}', $value, $this->action);
                    }
                }
                
                if ( $isCached ){
                    $datas[ $hash ] = array('params'=> $this->params, 'args'=>$args, 'action'=>$this->action);
                   // SystemCache::set('routes', $datas);
                }
                
                break;
            }
        }
    }
    
    private static function routeMatches($route, $method, $path, $format, $domain){
        if ( $method === null || $route['method'] == '*' || $method == $route['method'] ){
            $args = array();

            $result = preg_match_all($route['pattern'], $path, $matches);
            if (!$result)
                return null;

            if ($route['static'] != null)
            	$args['path'] = $route['static'];
            
            foreach($matches as $i => $value){
                if ( $i === 0 )                    
                    continue;
                
                $name = $route['args'][$i - 1];
                $v = $value[0];
                
                if ($route['static'] != null){
                	$v = str_replace('{' . $name . '}', $v, $args['path']);
                	$name = 'path';
                }
                
                $args[ $name ] = $v;
            }
            
            $args['_METHOD'] = $method;
            return $args;
        }
        return null;
    }

    /**
     * @param string $action
     * @param array $args
     * @param string $method
     * @return null|string URL of action
     */
    public static function path($action = null, $args = array(), $method = '*'){
        $router = Pharos::getCurrent()->getRouter();
        
        if ($action == null){
        	$action = $router->action;
        }
        
        if (!is_array($args)){
        	$args = $router->args;
        	unset($args["_METHOD"]);
        }
        
        return $router ? $router->reverse($action, $args, $method) : null;
    }
}
