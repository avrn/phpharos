<?php

namespace PHPharos\Providers;


use PHPharos\Http\Response;

class FenomTemplateProvider extends ResponseProvider {

	const CLASS_TYPE = '\\Fenom\\Template';
    
    /** 
     * @var \Fenom\Template 
     */
    private $template;
    private $args;
    
    public function __construct(Response $response) {
        parent::__construct($response);
        $this->template = $response->getEntity();
        $this->args = $response->getArgs();
        $response->setContentType( 'text/html' );
		$response->setHeader("Date", gmdate("D, d M Y H:i:s")." GMT");
        $response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s")." GMT");
    }
    
    public function onBeforeRender(){
        
    }


    public function getContent() {
        return $this->template->fetch($this->args);
    }

    public function render() {
        $this->template->display($this->args);
    }
}