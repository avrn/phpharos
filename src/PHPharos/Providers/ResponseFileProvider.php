<?php

namespace PHPharos\Providers;

use PHPharos\Http\Response;

use PHPharos\Http\MimeTypes;
use PHPharos\Http\Request;

class ResponseFileProvider extends ResponseProvider {

    const CLASS_TYPE = "\\PHPharos\\Providers\\FileResponse";

    public $cached = false;
    
    public function __construct(Response $response) {
        parent::__construct($response);

        $request = Request::getInstance();

        /** @var $file \PHPharos\Providers\FileResponse */
        $file = $response->getEntity();
        $response->setContentType( MimeTypes::getByExt($file->file->getExtension()) );

        $etag = hash_file('md5', $file->file->getPath()); //md5($file->file->lastModified());
        $response->cacheETag($etag);
        if (!$file->attach && $request->isCachedEtag($etag)){
            $response->setStatus(304);
            $this->cached = true;
        } else {
            $response->applyHeaders(array(
                'Content-Description' => 'File Transfer',
                'Content-Transfer-Encoding' => 'binary',
                'Pragma' => 'public',
                'Content-Length' => $file->file->length()
            ));

            if ($file->attach){
                $response->setHeader('Expires', '0');
                $response->setHeader('Cache-Control', 'must-revalidate');
                $response->setHeader('Content-Disposition', 'attachment; filename=' . urlencode( $file->file->getName() ));
            }
        }
    }

    public function onBeforeRender(){}
    
    public function render(){
        if (!$this->cached){
        	ob_flush();
            flush();
            readfile($this->response->getEntity()->file->getPath());
        }
    }
}
