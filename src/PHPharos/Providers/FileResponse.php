<?php

namespace PHPharos\Providers;


use PHPharos\Commons\File;

class FileResponse {

    public $attach;

    /**
     * @var File
     */
    public $file;

    public function __construct($file, $attach = true){
        if(!($file instanceof File))
            $file = new File($file);

        $this->file   = $file;
        $this->attach = $attach;
    }
}
