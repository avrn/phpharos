<?php

namespace PHPharos\Commons;

class File {

    private $path;
    private $extension;

    /**
     * @param string $path
     * @param \regenix\lang\File $parent
     */
    public function __construct($path, $parent = null){
        if ($parent instanceof File){
            $this->path = $parent->getPath() .'/'. $path;
        } else if (is_string($parent)){
        	$this->path = $parent .'/'. $path;
        } else
            $this->path = $path;
        
        while (Strings::contains($this->path, '//'))
        	$this->path = str_replace('//', '/', $this->path);
    }

    /**
     * @param null|string $prefix
     * @return File
     */
    public static function createTempFile($prefix = null){
        if ($prefix === null){
            $prefix = substr(md5(time()), 0, 5);
        }

        $path = tempnam(sys_get_temp_dir(), $prefix);
        return new File($path);
    }

    /**
     * @param $handle
     * @return File
     */
    public static function createFromHandle($handle){
        $file = new File(null);
        $file->handle = $handle;
        return $file;
    }

    /**
     * @param File $baseFile
     * @return mixed
     */
    public function getRelativePath(File $baseFile){
        $base = str_replace(array('\\', '////', '///', '//'), '/', $baseFile->getPath());
        $path = str_replace(array('\\', '////', '///', '//'), '/', $this->path);

        if (strpos($path, $base) === 0){
            return substr($path, strlen($base));
        } else
            return $path;
    }

    /**
     * get basename of file
     * @param null $suffix
     * @return string
     */
    public function getName($suffix = null){
        return basename( $this->path, $suffix );
    }

    /**
     * get basename of file without ext
     * @return string
     */
    public function getNameWithoutExtension(){
        return $this->getName('.' . $this->getExtension());
    }

    /**
     * get original path of file
     * @return string
     */
    public function getPath(){
        return $this->path;
    }

    /**
     * get parent directory as file object
     * @return File
     */
    public function getParentFile(){
        return new File(dirname($this->path));
    }

    /**
     * get parent directory as string
     * @return string
     */
    public function getParent(){
        return dirname($this->path);
    }

    /**
     * get real path of file
     * @return string
     */
    public function getAbsolutePath(){
        return realpath( $this->path );
    }

    /**
     * get real path as file object
     * @return File
     */
    public function getAbsoluteFile(){
        return new File($this->getAbsolutePath());
    }

    /**
     * get file extension in lower case
     * @return string file ext
     */
    public function getExtension(){
        if ( $this->extension !== null)
            return $this->extension;

        $p = strrpos( $this->path, '.' );
        if ( $p === false )
            return $this->extension = '';

        return $this->extension = strtolower(substr( $this->path, $p + 1 ));
    }

    /**
     * return true if file has passed extension
     * @param $extension
     * @return bool
     */
    public function hasExtension($extension){
        if ($extension[0] === '.')
            $extension = substr($extension, 1);

        return strtolower($extension) === $this->getExtension();
    }

    /**
     * return true if one of extensions is extension of file
     * @param array $extensions
     * @return bool
     */
    public function hasExtensions(array $extensions){
        foreach($extensions as $extension){
            if ($this->hasExtension($extension))
                return true;
        }
        return false;
    }

    /**
     * check file exists
     * @return boolean
     */
    public function exists(){
        return file_exists($this->path);
    }

    /**
     * @return boolean
     */
    public function canRead(){
        return is_readable($this->path);
    }

    /**
     * @return bool
     */
    public function isFile(){
        return is_file($this->path);
    }

    /**
     * @return bool
     */
    public function isDirectory(){
        return is_dir($this->path);
    }

    /**
     * @param File $new
     * @return bool
     */
    public function renameTo(File $new){
        return rename($this->getAbsolutePath(), $new->getPath());
    }

    /**
     * recursive create dirs
     * @return boolean - create dir
     */
    public function mkdirs(){
        if ( $this->exists() )
            return false;

        return mkdir($this->path, 0777, true);
    }

    /**
     * non-recursive create dirs
     * @return boolean
     */
    public function mkdir(){
        if ( $this->exists() )
            return false;

        return mkdir($this->path, 0777, false);
    }

    /**
     * @return integer get file size in bytes
     */
    public function length(){
        if ( $this->isFile() ){
            return filesize($this->path);
        } else
            return -1;
    }

    /**
     * Get last modified of file or directory in unix time format
     * @param bool $absolute
     * @param null|callable $filter
     * @return int unix time
     */
    public function lastModified($absolute = true, $filter = null){
        if ($absolute && $this->isDirectory()){
            $files  = $this->findFiles();
            $result = filemtime($this->path);
            foreach($files as $file){
                if ($filter && !call_user_func($filter, $file))
                    continue;

                $m_time = $file->lastModified($absolute, $filter);
                if ($m_time > $result)
                    $result = $m_time;
            }
            return $result;
        } else
            return filemtime($this->path);
    }

    /**
     * Check file or directory modified by current time stamp
     * @param int $currentTime unix time
     * @param bool $absolute
     * @return bool
     */
    public function isModified($currentTime, $absolute = true){
        if ($absolute && $this->isDirectory()){
            $result = filemtime($this->path);
            if ($result > $currentTime)
                return true;

            $files  = $this->findFiles();
            foreach($files as $file){
                $m_time = $file->lastModified();
                if ($m_time > $currentTime)
                    return true;
            }
            return false;
        } else
            return $currentTime < filemtime($this->path);
    }

    /**
     * Delete file or recursive remove directory
     * @return bool
     */
    public function delete($andParentsIfEmpty = false){
    	if (!$this->exists())
    		return false;
    	
        if (is_dir($this->path)){
            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($this->path, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::CHILD_FIRST
            );

            foreach ($files as $fileInfo) {
                $todo = ($fileInfo->isDir() ? 'rmdir' : 'unlink');
                @$todo($fileInfo->getRealPath());
            }
           
            @rmdir($this->path);
        } else {
            @unlink($this->path);
        }
        
        if ($andParentsIfEmpty == true){
        	$parent = $this->getParentFile();
        	
        	if ($parent->isDirectory() && $parent->exists()){
        		//double for refresh
        		$files = $parent->find();
        		$files = $parent->find();
        		if (count($files) == 0)
        			$parent->delete(true);
        	}
        }
        
        return !file_exists($this->path);
    }
    

    private $handle = null;

    private function fileNotOpen(File $file){
    	throw new \RuntimeException(sprintf('File "%s" is not opened to read or write', $file->getPath()));
    }
    
    /**
     * @param string $mode
     * @throws FileIOException
     * @throws CoreException
     * @return resource
     */
    public function open($mode){
        if ($this->handle)
            throw new \RuntimeException(sprintf('File "%s" is already opened, close the file before opening', $this->getPath()));

        $handle = fopen($this->path, $mode);
        if (!$handle)
            throw new \RuntimeException(sprintf('File "%s" can\'t read', $file->getPath()));

        $this->handle = $handle;
        return $handle;
    }

    /**
     * @param int $length
     * @return string
     * @throws FileNotOpenException
     * @return string
     */
    public function gets($length = 4096){
        if ($this->handle)
            return fgets($this->handle, $length);
        else
            $this->fileNotOpen($this);
    }

    /**
     * @param int $length
     * @return string
     * @throws FileNotOpenException
     * @return string
     */
    public function read($length = 4096){
        if ($this->handle)
            return fread($this->handle, $length);
        else
           $this->fileNotOpen($this);
    }

    /**
     * Open file and read all lines with callback
     * @param $callback
     * @param int $length
     * @throws \regenix\exceptions\FileNotOpenException
     */
    public function readLines($callback, $length = 4096){
        $this->open('r+');
        while (($buffer = $this->gets($length)) !== false) {
            call_user_func($callback, $buffer);
        }
        $this->close();
    }

    /**
     * @param $data
     * @param null $length
     * @return int
     * @throws FileNotOpenException
     */
    public function write($data, $length = null){
        if ($this->handle)
            // wtf php ?
            return $length !== null
                ? fwrite($this->handle, (string)$data, $length)
                : fwrite($this->handle, (string)$data);
        else
            $this->fileNotOpen($this);
    }

    /**
     * @param $offset
     * @param int $whence
     * @return int
     * @throws FileNotOpenException
     */
    public function seek($offset, $whence = SEEK_SET){
        if ($this->handle)
            return fseek($this->handle, (int)$offset, $whence);
        else
            $this->fileNotOpen($this);
    }

    /**
     * @return bool
     * @throws FileNotOpenException
     */
    public function isEof(){
        if ($this->handle)
            return feof($this->handle);
        else
            $this->fileNotOpen($this);
    }

    /**
     * @return bool
     */
    public function isOpened(){
        return is_resource($this->handle);
    }

    /**
     * @throws FileNotOpenException
     * @return bool
     */
    public function close(){
        if ($this->handle)
            return fclose($this->handle);
        else
            $this->fileNotOpen($this);
    }

    /**
     * @param null|int $flags
     * @return string
     */
    public function getContents($flags = null){
        if ($this->exists() && $this->isFile())
            return file_get_contents($this->path, $flags);
        else
            return '';
    }

    /**
     * @param $content
     * @param string $mode
     * @return int
     */
    public function putContents($content, $mode = 'w'){
        $this->open($mode);
        $result = $this->write($content);
        $this->close();
        return $result;
    }

    /**
     * @param null|int $time
     * @param null|int $atime
     * @return bool
     */
    public function touch($time = null, $atime = null){
        return touch($this->path, $time, $atime);
    }


    /**
     * find files and dirs
     * @return string[]
     */
    public function find(){
        $path = $this->path;
        if (substr($path, -1) !== '/')
            $path .= '/';

        $files = scandir($path);
        $result = array();
        foreach($files as $file){
            if ($file != '..' && $file != '.')
                $result[] = $path . $file;
        }
        return $result;
    }

    /**
     * @param bool $recursive
     * @return File[]
     */
    public function findFiles($recursive = false){
        $files = $this->find();
        if ($files){
            foreach($files as &$file){
                $file = new File($file);
            }
            unset($file);
        }
        if ($recursive){
            $addFiles = array();
            /** @var $file File */
            foreach($files as $file){
                if ($file->isDirectory()){
                    $addFiles = array_merge($addFiles, $file->findFiles(true));
                }
            }
            $files = array_merge($files, $addFiles);
        }
        return $files;
    }

    public function __toString() {
        return $this->path;
    }

    public static function sanitize($fileName){
        return preg_replace('/[^a-zA-Z0-9-_\.]/', '', $fileName);
    }
}
