<?php

namespace PHPharos\Commons;

final class Strings {

	const ENCODING = 'utf-8';
	
    private function __construct(){}

    /**
     * @param string $string
     * @return string
     */
    public static function format($string){
        $args = func_get_args();
        return vsprintf($string, array_slice($args, 1));
    }

    /**
     * @param string $string
     * @param array $args
     * @return string
     */
    public static function formatArgs($string, array $args = array()){
        return vsprintf($string, $args);
    }

    /**
     * @param $string
     * @param $from
     * @param null $to
     * @return string
     */
    public static function substring($string, $from, $to = null){
        if ($to === null)
            return mb_substr($string, $from, null, self::ENCODING);
        else
            return mb_substr($string, $from, $to - $from, self::ENCODING);
    }

    /**
     * return true if sting start with
     * @param string $string
     * @param string $with
     * @return boolean
     */
    public static function startsWith($string, $with){
        return strpos($string, $with) === 0;
    }

    
    public static function replace($search, $replace, $string){
    	return str_replace($search, $replace, $string);
    }
    
    /**
     *
     * @param string $string
     * @param string $with
     * @return boolean
     */
    public static function endsWith($string, $with){
        // TODO optimize ?
        return substr($string, -strlen($with)) === $with;
    }

    public static function contains($string, $what){
    	return strpos($string, $what) !== false;
    }
    
    private static $alpha   = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    private static $numeric = '0123456789';
    private static $symbol  = '~!@#$%^&*+/-*_=';

    /**
     * @param int $length
     * @param bool $withNumeric
     * @param bool $withSpecSymbol
     * @return string
     */
    public static function random($length, $withNumeric = true, $withSpecSymbol = false){
        $characters = self::$alpha;
        if ($withNumeric)
            $characters .= self::$numeric;
        if ($withSpecSymbol)
            $characters .= self::$symbol;

        $randomString = '';
        $len = strlen($characters);
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, $len - 1)];
        }
        return $randomString;
    }

    /**
     * @param $lengthFrom
     * @param $lengthTo
     * @param bool $withNumeric
     * @param bool $withSpecSymbol
     * @return string
     */
    public static function randomRandom($lengthFrom, $lengthTo, $withNumeric = true, $withSpecSymbol = false){
        $length = mt_rand($lengthFrom, $lengthTo);
        return self::random($length, $withNumeric, $withSpecSymbol);
    }
}