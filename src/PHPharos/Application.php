<?php

namespace PHPharos;

use PHPharos\Providers\ResponseProvider;

use PHPharos\Routing\RouterConfiguration;

use PHPharos\Routing\Router;

use PHPharos\Http\Request;

use PHPharos\Config\PropertiesConfiguration;

use PHPharos\Http\URL;

use PHPharos\Commons\File;

use PHPharos\Logger\FileLogger;
use PHPharos\Logger\Logger;
use PHPharos\Exceptions\TemplateNotFoundException;

/**
 * Application dependency container
 *
 * --@property-read \PHPixie\DB $db Database module
 * --@property-read \PHPixie\ORM $orm ORM module
 */
class Application {
	
	private $name;
	private $path;
	/**
	 * @var \Fenom $fenom
	 */
	private $fenom;
	private $isDev = true;

	/**
	 * @var URL
	 */
	private $currentRule;
	
	/**
	 * @var PropertiesConfiguration
	 */
	private $configuration;
	
	/**
	 * @var AbstractBootstrap
	 */
	protected $bootstrap;
	
	protected $request;
	
	/**
	 * @var Router
	 */
	protected $router;
	
	public function __construct(File $path){
		$this->path = $path->getPath();
		$this->name = $path->getName();
		$this->router = new Router();
		
		$this->configuration = new PropertiesConfiguration(new File($path->getPath()."/conf/application.conf"));
	}

	public function getName(){
		return $this->name;
	}
	
	public function getPath(){
		return $this->path;
	}
	
	public function getPublicPath(){
		return $this->getPath() . '/public';	
	}
	
	
	/**
	 * @return \PHPharos\Routing\Router
	 */
	public function getRouter(){
		return $this->router;
	}
	
	/**
	 * @return \PHPharos\Http\URL|NULL
	 */
	public function getCurrentRule(){
		
		if ($this->currentRule != null)
			return $this->currentRule;
		
		$request = Request::getInstance();
		$rules = $this->getRules();
		foreach ($rules as $url){
			if ($request->isBase($url)){
				$this->currentRule = $url;
				return $url;
			}
				
		}
	
		return null;
	}
	
	
	
	public function isDev(){
		return $this->isDev;
	}
	
	public function setDev($value){
		$this->isDev = (bool)$value;
		
		if ($this->isDev)
			$this->getConfig()->setEnv("dev");
		else 
			$this->getConfig()->setEnv(false);
	}
	
	/**
	 * @return \PHPharos\Config\PropertiesConfiguration
	 */
	public function getConfig(){
		return $this->configuration;
	}
	
	/**
	 * @return \Fenom
	 */
	public function getFenom(){
		return $this->fenom;		
	}
	
	public function setBootstrap(AbstractBootstrap $bootstrap){
		$this->bootstrap = $bootstrap;
	}
	
	/**
	 * @return \PHPharos\AbstractBootstrap
	 */
	public function getBootstrap(){
		return $this->bootstrap;
	}
	
	public function registerLogger(){
		$path = new File($this->getConfig()->get("logger.path", "/log"), $this->getPath());
		$path->mkdirs();
		
		$file =  new File($this->getConfig()->get("logger.file", "system.log"), $path);
		Logger::init(new FileLogger($file->getPath()));
	}
	
	
	public function registerActiveRecord(){
		$conf = $this->getConfig();
		
		if (!$conf->getBoolean("activerecord", false))
			return;
		
		$path = new File($conf->get("activerecord.models.path", "/src/Models"), $this->getPath());
		$path->mkdirs();
	
		$connection = $conf->get("activerecord.connection");
		
		\ActiveRecord\Config::initialize(function($cfg) use ($path, $connection){
				$cfg->set_model_directory($path->getPath());
				$cfg->set_connections(
						array('default' => $connection),
						'default'
				);
		});
	}
	
	public function registerFenom(){
		$conf = $this->getConfig();
		
		$options = array_fill_keys($conf->getArray("fenom.options", array()), true); 
		
		$views_path = new File($conf->get("fenom.views.path", "/src/Views"), $this->getPath());
		$views_path->mkdirs();
		
		$tmp_path = new File($conf->get("fenom.tmp", "/tmp/views"), $this->getPath());
		$tmp_path->mkdirs();
		
		$this->fenom = \Fenom::factory($views_path->getPath(), $tmp_path->getPath(), $options);
		
		$this->fenom->addFunctionSmart("resource", "\\PHPharos\\TemplateFunctions::resource");
		$this->fenom->addModifier('replace', "\\PHPharos\\TemplateFunctions::replace");
		$this->fenom->addModifier('text', "\\PHPharos\\TemplateFunctions::text");
		$this->fenom->addModifier('url', "\\PHPharos\\TemplateFunctions::url");
		$this->fenom->addModifier('rub', "\\PHPharos\\TemplateFunctions::rub");
		$this->fenom->addFunctionSmart('route', "\\PHPharos\\TemplateFunctions::route");
		
		ResponseProvider::register('\\PHPharos\\Providers\\FenomTemplateProvider', '\\Fenom\\Template');
		ResponseProvider::register('\\PHPharos\\Providers\\FenomTemplateProvider', '\\Fenom\\Render');
	}
	
	/**
	 * @param string $name
	 * @param string $ext
	 * @throws RuntimeException
	 * @return \Fenom\Render
	 */
	public function loadTemplate($name, $ext = 'html'){
		try {
			$template = $this->getFenom()->getTemplate($name.".".$ext);
			return $template;
			
		} catch (\RuntimeException $e){
			if (strpos($e->getMessage(), "Template") === 0)
				throw new TemplateNotFoundException($e->getMessage());
			throw $e;
		}
	}
	
	public function registerRoutes(){
		$this->router = new Router();
		
		$routeFile = $this->getPath() . '/conf/route';
		$route = new File($routeFile);
		//$routePatternDir = new File($this->getPath() . 'conf/routes/');
	
		// routes
		$routeConfig = null;
		$routeConfigData = null; //SystemCache::get('route');
		if (is_array($routeConfigData)){
			$routeConfig = new RouterConfiguration();
			//$routeConfig->setPatternDir($routePatternDir);
			$routeConfig->setFile($route);
			$routeConfig->addPatterns($routeConfigData);
		}
	
		// optimize, absolute cache
/*		if (!$this->stat && $routeConfig !== null){
			--$this->loadRouting($routeConfig);
			$this->router->applyConfig($routeConfig);
			return;
		}*/
	
		$upd = null; //SystemCache::get('routes.$upd');
		if ( !is_array($routeConfig)
				|| $route->isModified($upd, false)
				//|| $routePatternDir->isModified($upd, REGENIX_IS_DEV) 
		){
	
			
			$routeConfig  = new RouterConfiguration();
	
			/*
			foreach (Module::$modules as $name => $module){
				$routeConfig->addModule($name, '.modules.' . $name . '.controllers.', $module->getRouteFile());
			}
			*/
			
			//$routeConfig->setPatternDir($routePatternDir);
			$routeConfig->setFile($route);
	
			$routeConfig->load();
			$routeConfig->validate();
	
			
	
			//SystemCache::setWithCheckFile('route', $routeConfig->getRouters(), $routeFile, 60 * 5);
			//$upd = $routePatternDir->lastModified(REGENIX_IS_DEV);
			$updR = $route->lastModified();
			if ($updR > $upd)
				$upd = $updR;
	
			//SystemCache::set('routes.$upd', $upd);
		}
		
		$this->router->applyConfig($routeConfig);
		//$this->loadRouting($routeConfig);
	}
	



	public function getRules(){
		$rules = $this->getConfig()->getArray("app.rules", array("/"));
		
		$urls = array();
		foreach($rules as $rule){
			$urls[] = new URL(trim($rule));
		}
		
		return $urls;
	}
		
	
}
