<?php
namespace PHPharos;


use PHPharos\Providers\FileResponse;
use PHPharos\Providers\ResponseProvider;
use PHPharos\Http\Session\Flash;
use PHPharos\Http\Cookie;
use PHPharos\Http\Request;
use PHPharos\Exceptions\HttpException;
use PHPharos\Routing\Router;
use PHPharos\Commons\Strings;
use PHPharos\Http\Response;
use PHPharos\Http\Session\Session;
use PHPharos\Http\RequestBody;
use PHPharos\Validation\Validation;
use PHPharos\Validation\Validator;
use PHPharos\Validation\AbstractValidator;
use PHPharos\Validation\Exceptions\ValidationException;
use PHPharos\Exceptions\TemplateNotFoundException;

abstract class Controller {

    private $__data = array(
        'request' 		=> null,
        'session' 		=> null,
        'flash'   		=> null,
        'cookie'  		=> null,
        'query'   		=> null,
        'body'    		=> null,
    	'validation' 	=> null
    );

    /** @var Response */
    public $response;

    /** 
     * @var \PHPharos\Http\Request 
     */
    public $request;

   /**
    * @var \PHPharos\Http\Session\Session
    */
    public $session;

    /** 
     * @var \PHPharos\Http\Session\Flash 
     */
    public $flash;

    /** 
     * @var \PHPharos\Http\Cookie 
     */
    public $cookie;
    
    /** @var \PHPharos\Http\RequestQuery */
    public $query;

    /** 
     * @var \PHPharos\Http\RequestBody 
     */
    public $body;
    
    /**
     * @var \PHPharos\Validation\Validation
     */
    public $validation;

    /**
     * method name of invoke in request
     * @var string
     */
    public $actionMethod;

    /**
     * @var \ReflectionMethod
     */
    public $actionMethodReflection;

    /**
     * template arguments
     * @var array[string, any]
     **/
    private $renderArgs = array();

    /**
     * get current route arguments
     * @var array[string, any]
     */
    public $routeArgs = array();


    /**
     * @var bool
     */
    private $useSession = true;

    /**
     * @var Controller
     */
    private static $current;


    public function __construct() {
        $this->response = new Response();

        unset($this->body);
        unset($this->request);
        unset($this->cookie);
        unset($this->session);
        unset($this->flash);
        unset($this->query);
        unset($this->validation);
    }

    public function __get($name){
        if ($this->__data[$name])
            return $this->__data[$name];

        $value = null;
        switch($name){
            case 'body':    	$value = RequestBody::getInstance(); break;
			case 'request': 	$value = Request::getInstance(); break;
			case 'cookie':  	$value = Cookie::getInstance(); break;
            case 'session': 	$value = Session::getInstance(); break;
            case 'flash':   	$value = Flash::getInstance(); break;
            case 'validation':	$value = Validation::getInstance(); break;
           // case 'query':   $value = DI::getSingleton(RequestQuery::type); break;
           

            default: {
               // return parent::__get($name);
            }
        }

        return $this->__data[$name] = $value;
    }

    protected function onBefore(){}
    protected function onAfter(){}
    protected function onFinally(){}
    protected function onReturn($result){}

    protected function onException(\Exception $e){}
    protected function onHttpException(HttpException $e){}

    /**
     * @param $params
     */
    protected function onBindParams(&$params){}
    
    public function callBefore(){
        $this->onBefore();
    }
    
    public function callAfter(){
        $this->onAfter();
    }
    
    public function callFinally(){
        $this->onFinally();
        if ($this->useSession){
            $this->flash->touchAll();
        }
    }

    public function callReturn($result){
        $this->onReturn($result);
    }

    public function callBindParams(&$params){
        $this->onBindParams($params);
    }

    /**
     * set use session, if true use session and flash features
     * default: true
     * @param bool $value
     */
    protected function setUseSession($value){
        $this->useSession = (bool)$value;
    }
    
    final public function callException(\Exception $e){
        $this->onException($e);
    }

    final public function callHttpException(HttpException $e){
        $this->onHttpException($e);
    }

    /**
     * put a variable for template
     * @param string $varName
     * @param mixed $value
     * @return Controller
     */
    public function put($varName, $value){
        $this->renderArgs[ $varName ] = $value;
        return $this;
    }

    /**
     * put a variables for template
     * @param array $vars
     * @return Controller
     */
    public function putAll(array $vars){
        foreach ($vars as $key => $value) {
            $this->put($key, $value);
        }
        return $this;
    }

    public function send(){
        throw new Result($this->response);
    }

    private function __redirectUrl($url, $permanent = false){
        $this->response
            ->setStatus($permanent ? 301 : 302)
            ->setHeader('Location', $url);
        $this->send();
    }

    /**
     * @param $url
     * @param bool $permanent
     */
    public function redirectUrl($url, $permanent = false){
        if (Strings::startsWith($url, "/")){
            $app = Pharos::getCurrent();
            $url = $app->getCurrentRule()->getPath().'/'.$url; //$app->getUriPath($url);
            while (Strings::contains($url, '//'))
            	$url = Strings::replace('//', '/', $url);
        }
        $this->__redirectUrl($url, $permanent);
    }

    /**
     * @param $action
     * @param array $args
     * @param bool $permanent
     */
    public function redirect($action, array $args = array(), $permanent = false){
    	
        if (strpos($action, '.') === false)
            $action = '.' . get_class($this) . '.' . $action;

        $url = Router::path($action, $args, 'GET');
        if ($url === null)
            throw new \RuntimeException(Strings::format('Can`t reverse url for action "%s(%s)"', $action, implode(', ', array_keys($args))));

        $this->__redirectUrl($url, $permanent);
    }

    /**
     * redirect to current open url
     * @param array $args
     * @param bool $permanent
     */
    public function refresh(array $args = null, $permanent = false){
    	if ($args == null){
    		$args = $this->routeArgs;
    		unset($args['_METHOD']);
    	}
    	
        $this->redirect($this->actionMethod, $args, $permanent);
    }

 	
    public function validate($entity, $entityName = null){
    	
    	if ($entity instanceof Validator){
    		$validator = $entity->validator();
    		
    	} else if ($entity instanceof AbstractValidator){
    		$validator = $entity;
    	} else 
    		throw new ValidationException('%s must be instance of Validator', $entity);
    	
    	if ($validator->hasErrors())
	    	foreach ($validator->getErrors() as $e){
	    		$this->validation->addError($e->getAttribute($entityName), $e->message);
	    	}
    }

    /**
     * Work out the default template to load for the invoked action.
     * E.g. "controllers\Pages\index" returns "views/Pages/index.html".
     */
    public function template($template = false){
        if (!$template){
        	$back = debug_backtrace();
        	$action = $back[3]['function'];
        	$class = $back[3]['class'];
            //$class = $this->actionMethodReflection->getDeclaringClass()->getName();
            $controller = str_replace('\\', '/', $class);

            if ( Strings::startsWith($controller, 'Controllers/') )
                $controller = substr($controller, 12);

            $template   = $controller . '/' . $action; //$this->actionMethod;
        }
        return str_replace('\\', '/', $template);
    }

    public function renderToString($template = false, array $args = null){
    	return $this->renderTemplate($template, $args, false);
    }
    
    /**
     * Render the corresponding template
     * render template by action method name or template name
     * @param bool|object|string $template
     * @param array $args
     */
    public function render($template = false, array $args = null){
        if (is_object($template)){
        	$this->response->setArgs($this->renderArgs);
            $this->response->setEntity($template);
            $this->send();
        } else
            $this->renderTemplate($template, $args);
    }

    /**
     * Render a specific template.
     * @param $template
     * @param array $args
     */
    private function renderTemplate($templateName, array $args = null, $send = true){
        if ( $args )
            $this->putAll($args);

        $templateName = $this->template($templateName);

        $this->put("flash", $this->flash);
        $this->put("session", $this->session);
        $this->put("request", $this->request);
        $this->put("validation", $this->validation);
        $this->put("application", Pharos::getCurrent());
     
        try {
        	$template = Pharos::getCurrent()->loadTemplate($templateName);
        	$this->response->setArgs($this->renderArgs);
        	$this->response->setEntity($template);
        	
        	if ($send)
        		$this->send();
        	else 
        		return $this->response->getContent();
        } catch (TemplateNotFoundException $e){
        	$this->notFound($e->getMessage());
        }
    }

    public function renderText($text){
        $this->response->setEntity( $text );
        $this->send();
    }
    
    public function renderHtml($html){
        $this->response
                ->setContentType('text/html')
                ->setEntity($html);
        
        $this->send();
    }

    public function renderJson($object){
        $this->response
                ->setContentType('application/json')
                ->setEntity( json_encode($object) );
        
        $error = json_last_error();
        if ( $error > 0 ){
            throw new \RuntimeException('Error json encode, ' . $error);
        }
        
        $this->send();
    }

    public function renderJsonp($object){
        $callback = $this->query->get('jsonp');
        if (!$callback)
            $callback = $this->query->get('callback');
        if (!$callback)
            $callback = 'callback' . rand(0, 99999);

        $this->response
            ->setContentType('application/javascript')
            ->setEntity( $callback . '(' . json_encode($object) . ')' );

        $error = json_last_error();
        if ( $error > 0 ){
            throw new \RuntimeException('Error json encode, ' . $error);
        }

        $this->send();
    }
    
    public function renderXml($xml){
    	$this->setUseSession(false);
    	 
    	$this->response->setContentType('text/xml');
		
        if ( $xml instanceof \SimpleXMLElement ){
            /** @var \SimpleXMLElement */
            $this->response->setEntity($xml->asXML());
        } else 
			$this->response->setEntity($xml);
		
		$this->send();
    }

    /**
     * @param File|string $file
     * @param bool $attach
     */
    
    public function renderFile($file, $attach = true){
        $this->setUseSession(false);
        // TODO optimize ?
        ResponseProvider::register("\\PHPharos\\Providers\\ResponseFileProvider", "\\PHPharos\\Providers\\FileResponse");
        $this->response->setEntity(new FileResponse($file, $attach));
        $this->send();
    }
	
    
    /**
     * render print_r var if dev
     * @param $var
     */
    public function renderVar($var){
        $this->renderHtml('<pre>' . print_r($var, true) . '</pre>');
    }

    /**
     * render var_dump var if dev
     * @param $var
     */
    public function renderDump($var){
        ob_start();
        var_dump($var);
        $str = ob_get_contents();
        ob_end_clean();

        $this->renderHtml($str);
    }

    public function ok(){
        $this->response->setStatus(200);
        $this->send();
    }

    /**
     * @param string $message
     * @throws \PHPharos\Exceptions\HttpException
     */
    public function forbidden($message = ''){
        throw new HttpException(HttpException::E_FORBIDDEN, $message);
    }

    /**
     * @param string $message
     * @throws \PHPharos\Exceptions\HttpException
     */
    public function notFound($message = ''){
		throw new HttpException(HttpException::E_NOT_FOUND, $message, $this->renderArgs);
    }
    
    /**
     * @param string $message
     * @throws \PHPharos\Exceptions\HttpException
     */
    public function error($message = ''){
    	throw new HttpException(HttpException::E_INTERNAL_SERVER_ERROR, $message);
    }

    /**
     * Can use several what, notFoundIfEmpty(arg1, arg2, arg3 ...)
     * @param mixed $whats..
     */
    public function notFoundIfEmpty($whats){
        $args = func_get_args();
        foreach($args as $arg){
            if (empty($arg))
                $this->notFound();
        }
    }

    /**
     * If not ajax request to 404 exception
     */
    public function forAjaxOnly(){
        if (!$this->request->isAjax())
            $this->notFound('For ajax only');
    }
}