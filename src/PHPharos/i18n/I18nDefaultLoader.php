<?php
namespace PHPharos\i18n;

use PHPharos\Pharos;
use PHPharos\Config\PropertiesConfiguration;
use PHPharos\Commons\File;

class I18nDefaultLoader implements I18nLoader {

    protected function getLangFile($lang){
        $app = Pharos::getCurrent();
        return $app->getPath() . '/conf/i18n/' . $lang . '.lang';
    }

    public function loadLang($lang) {
        $file = self::getLangFile($lang);
        if ( file_exists($file) ){
            $messages = null; //SystemCache::getWithCheckFile('i18n.' . $lang, $file);
            if ( $messages === null ){
                $config   = new PropertiesConfiguration(new File($file));
                $messages = $config->all();
                //SystemCache::setWithCheckFile('i18n.' . $lang, $messages, $file);
            }
            I18n::setMessages($lang, $messages);
            return true;
        }
        return false;
    }

    public function getLastUpdate($lang){
        $file = self::getLangFile($lang);
        if (file_exists($file))
            return filemtime($file);
        else
            return -1;
    }
}