<?php
namespace PHPharos\i18n;

use PHPharos\Http\Session\Session;
use PHPharos\Http\Request;
use PHPharos\Pharos;
use regenix\mvc\http\RequestQuery;

class I18n {

    private static $messages = array();
    private static $lang = 'default';

    private static $detectType = 'none';
    private static $detectArg  = '_lang';

    public static function setMessages($lang, $messages){
        self::$messages[$lang] = $messages;
    }

    public static function getMessages($lang){
        if (!self::$messages[$lang]){
            self::$loader->loadLang($lang);
        }
        return self::$messages[$lang];
    }

    public static function setLang($lang, $save = true){
        self::$lang = str_replace(array(' ', '-'), '_', strtolower($lang));

        if ( $save ){
            switch(self::$detectType){
                case 'session': {
                    $session = Session::getInstance();
                    $session->put( self::$detectArg, self::$lang );
                } break;
            }
        }
    }

    public static function getLang(){
        return self::$lang;
    }

    /**
     * @param $lang
     * @return mixed
     */
    public static function availLang($lang = null){
        if ( $lang === null ){
            $lang = self::detectLang(Request::getInstance());
            return !$lang || self::availLang($lang);
        } else
            return isset(self::$loader) && (isset(self::$messages[$lang]) || self::$loader->loadLang($lang));
    }


    /**
     * return unique hash of lang from last update
     * @param string $lang
     * @return string
     */
    public static function getLangStamp($lang){
        return md5(self::$loader->getLastUpdate($lang));
    }

    /**
     * i18n format string
     * @param string $message
     * @param array $args
     * @return string
     */
    public static function format($message, array $args){
        $keys = array_map(function($key){
            return '{' . $key . '}';
        }, array_keys($args));

        return str_replace($keys, $args, $message);
    }

    /**
     * @param $message
     * @param string|array $args
     * @return string
     */
    public static function get($message, $args = ''){
        $lang = self::$lang;

        if ( !isset(self::$messages[$lang]) )
            self::$loader->loadLang($lang);

        if (isset(self::$messages[ $lang ][$message]))
            $message = self::$messages[ $lang ][$message];

        if (is_array($args))
            return self::format($message, $args);
        else
            return self::format($message, array_slice(func_get_args(), 1));
    }

    /** @var I18nLoader */
    private static $loader;
    public static function setLoader(I18nLoader $loader){
        self::$loader = $loader;
    }

    /**
     * @param \regenix\mvc\http\Request $request
     * @return bool|\regenix\mvc\scalar|null|string
     */
    public static function detectLang(Request $request){
        $app = Pharos::getCurrent();

        $lang = false;
        switch(self::$detectType){
            case 'headers': {
                $languages = $request->getLanguages();
                foreach($languages as $lang){
                    if ( self::availLang($lang) ){
                        return $lang;
                    }
                }
            } break;
            case 'route': {
                $lang = $app->getRouter()->args[self::$detectArg];
            } break;
            case 'get': {
                $query = RequestQuery::getInstance();
                $lang = $query->getString(self::$detectArg);
            } break;
            case 'session': {
                $session = Session::getInstance();
                $lang = $session->get(self::$detectArg);
            } break;
            default: {
                $lang = $app->getConfig()->getString('i18n.lang', 'default');
            }
        }
        return $lang;
    }

    public static function initialize() {
        self::setLoader(new I18nDefaultLoader());

        $app = Pharos::getCurrent();
        $request = Request::getInstance();

        if ( isset($request) ){
            $type = $app->getConfig()->getString('i18n.detect.type', 'none');
            $arg  = $app->getConfig()->getString('i18n.detect.arg', '_lang');

            self::$detectType = $type;
            self::$detectArg  = $arg;

            $lang = self::detectLang($request);

            if ($lang && self::availLang($lang))
                self::setLang($lang);
        }
    }
}

I18n::initialize();
