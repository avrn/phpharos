<?php

namespace PHPharos;

abstract class AbstractBootstrap {

    /**
     * @var Application
     */
    protected $app;

    public function __construct(Application $app){
        $this->app = $app;
    }

    public function onBeforeLoad(){}
    public function onAfterLoad(){}

    public function onException(\Exception $e){}


}