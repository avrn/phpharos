<?php
namespace PHPharos;

use PHPharos\Commons\File;

use PHPharos\Commons\Strings;

use PHPharos\Controller;

class StaticController extends Controller {

	public function load($path){

		if (!Strings::startsWith($path, '/'))
			$path = '/'.$path;
		
		$file = new File(Pharos::getCurrent()->getPath() . $path);
		
		if (!$file->exists())
			$this->notFound();
		
		$this->renderFile($file, false);
	}
}